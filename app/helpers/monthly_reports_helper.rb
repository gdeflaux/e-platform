module MonthlyReportsHelper
  def percentage(boys, girls, gender)
    total = boys + girls
    value = (gender == :boys) ? boys : girls
    return "%.2f" % (value.to_f / total.to_f * 100)
  end
end
