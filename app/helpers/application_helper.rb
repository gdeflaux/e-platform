# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  
  def layout_title
    if (request.request_uri =~ /^\/admin(\/)?/) != nil
      "CRC Afghanistan - Administration"
    elsif (request.request_uri =~ /^\/reporting(\/)?/) != nil
      "CRC Afghanistan - Reporting"
    else
      "CRC Afghanistan | "
    end
  end

  def navigation_items
    if !defined?(@navigation_items)
      if (request.request_uri =~ /^\/admin(\/)?/) != nil
        @navigation_items = [[/(^\/admin$)|(admin\/users)/, admin_users_url, "Users"],
                            [/admin\/services/, admin_services_url, "Services"],
                            [/admin\/categories/, admin_categories_url, "Categories"],
                            [/admin\/languages/, admin_languages_url, "Languages"],
                            [/admin\/configurations/, admin_configurations_url, "Configuration"]]
      elsif (request.request_uri =~ /^\/reporting(\/)?/) != nil
        @navigation_items = [[/(^\/reporting$)|(reporting\/monthly_reports)/, reporting_monthly_reports_url, "Monthly Reports"],
                            [/reporting\/(axes|activities)/, reporting_axes_url, "Axes & Activities"],
                            [/reporting\/crc_members/, reporting_crc_members_url, "CRC Members"]]
      else
        @navigation_items = [[/(^\/$)|(news)/, newsitems_url, "News"],
                            [/statics/,"/statics/about-crc", "About CRC"],
                            [/reports/, reports_url, "Reports"],
                            [/partners/, partners_url, "Partners"],
                            [/documents/, documents_url, "Documents"],
                            [/service_providers/, service_providers_url, "Service Providers"],
                            [/links/, links_url, "Links"]]
      end
    end
    @navigation_items
  end
  
  def render_aside_blocks
    if (request.request_uri =~ /^\/admin(\/)?/) != nil
      render_aside_blocks_admin
    elsif (request.request_uri =~ /^\/reporting(\/)?/) != nil
      render_aside_blocks_reporting
    else
      render_aside_blocks_public
    end
  end
  
  def render_aside_blocks_public
    if (controller_name =~ navigation_items[0][0]) != nil
      render_aside_blocks_home
    elsif (controller_name =~ navigation_items[1][0]) != nil
      render_aside_blocks_static
    elsif (controller_name =~ navigation_items[2][0]) != nil
      render_aside_blocks_reports
    elsif (controller_name =~ navigation_items[3][0]) != nil
      render_aside_blocks_partners
    elsif (controller_name =~ navigation_items[4][0]) != nil
      render_aside_blocks_documents
    elsif (controller_name =~ navigation_items[5][0]) != nil
      render_aside_blocks_service_providers
    elsif (controller_name =~ navigation_items[6][0]) != nil
      render_aside_blocks_links
    elsif (controller_name =~ /contact_us/) != nil
      render_aside_blocks_contact_us
    end
  end
  
  def render_aside_blocks_admin
    render(:partial => "shared/site_statistics", :layout => "shared/aside_block")
  end
  
  def render_aside_blocks_reporting
    ""
  end
  
  def render_aside_blocks_home
    if action_name =~ /(new|create|edit|update)/
      html = render :partial => "shared/asset_upload_form", :layout => "shared/aside_block", :locals => {:action => "upload_asset", :content => @newsitem}
      html += render :partial => "shared/asset_list", :object => @newsitem, :layout => "shared/aside_block"
    else
      html = render(:partial => "newsitems_categories", :layout => "shared/aside_block")
      html += render(:partial => "shared/moi", :layout => "shared/aside_block")
      html += render(:partial => "shared/documents_highlighted", :layout => "shared/aside_block")
    end
    html
  end
  
  def render_aside_blocks_static
     if action_name =~ /(new|create|edit|update)/
        html = render :partial => "shared/asset_upload_form", :layout => "shared/aside_block", :locals => {:action => "upload_asset", :content => @static}
        html += render :partial => "shared/asset_list", :object => @static, :layout => "shared/aside_block"
      else
        html = render(:partial => "shared/afgha_stats", :layout => "shared/aside_block")
        html += render(:partial => "shared/moi", :layout => "shared/aside_block")
        html += render(:partial => "shared/documents_highlighted", :layout => "shared/aside_block")
      end
      html
  end
  
  def render_aside_blocks_reports
    html = render :partial => "shared/newsitems_latest", :layout => "shared/aside_block"
    html += render(:partial => "shared/documents_highlighted", :layout => "shared/aside_block")
    html += render(:partial => "shared/moi", :layout => "shared/aside_block")
  end
  
  def render_aside_blocks_partners
    html = ""
    if action_name =~ /(new|create|edit|update)/
      html = render :partial => "shared/asset_upload_form", :layout => "shared/aside_block", :locals => {:action => "upload_asset", :content => @partner}
      html += render :partial => "shared/asset_list", :object => @partner, :layout => "shared/aside_block"
    else
      html = render :partial => "shared/newsitems_latest", :layout => "shared/aside_block"
      html += render(:partial => "shared/moi", :layout => "shared/aside_block")
      html += render(:partial => "shared/documents_highlighted", :layout => "shared/aside_block")

    end
    html
  end
  
  def render_aside_blocks_links
    if action_name !~ /(new|create|edit|update)/
      html = render :partial => "shared/newsitems_latest", :layout => "shared/aside_block"
      html += render(:partial => "shared/moi", :layout => "shared/aside_block")
      html += render(:partial => "shared/documents_highlighted", :layout => "shared/aside_block")
    end
  end
  
  def render_aside_blocks_documents
    if action_name =~ /(new|create|edit|update)/
      html = render :partial => "shared/asset_upload_form", :layout => "shared/aside_block", :locals => {:action => "upload_asset", :content => @document}
      html += render :partial => "shared/asset_list", :object => @document, :layout => "shared/aside_block"
    else
      html = render(:partial => "documents_categories", :layout => "shared/aside_block")
      html += render :partial => "shared/newsitems_latest", :layout => "shared/aside_block"
      html += render :partial => "shared/partner_focus", :layout => "shared/aside_block"
    end
  end
  
  def render_aside_blocks_service_providers
    if action_name !~ /(new|create|edit|update)/
      html = render(:partial => "service_providers_categories", :layout => "shared/aside_block")
      html += render :partial => "shared/newsitems_latest", :layout => "shared/aside_block"
      html += render(:partial => "shared/documents_highlighted", :layout => "shared/aside_block")
      html += render(:partial => "shared/moi", :layout => "shared/aside_block")
    end
  end
  
  def render_aside_blocks_contact_us
    html = render :partial => "shared/newsitems_latest", :layout => "shared/aside_block"
    html += render(:partial => "shared/documents_highlighted", :layout => "shared/aside_block")
    html += render(:partial => "shared/moi", :layout => "shared/aside_block")
  end
  
  def category_options(type, sel = "")
    categories = Category.find(:all, :conditions => {:parent => type}).collect { |c| [c.name, c.id] }
    options_for_select(categories, {:selected => sel})
  end
  
  def set_page_description (description = "")
    @page_description = description
  end
  
  def set_page_title (title = "")
    @page_title = title
  end
  
  def no_thumbnail
    "/images/no_thumb.png"
  end
  
  def audio_thumbnail
    "/images/audio_file.png"
  end
  
  def path_to_url(path)
    request.protocol + request.host_with_port + path
  end
  
  def custom_head_tags(args = {})
    if args[:js]
      if !defined?(@js_includes)
        @js_includes = Array.new(args[:js])
      else
        @js_includes = @js_includes + args[:js]
      end
    end
    
    if args[:css]
      if !defined?(@css_includes)
        @css_includes = Array.new(args[:css])
      else
        @css_includes = @css_includes + args[:css]
      end
    end
    
    {:js => @js_includes, :css => @css_includes}
  end
  
  def print_custom_head_tags
    tags = ""
    if custom_head_tags()[:js]
      custom_head_tags()[:js].each { |js| tags += javascript_include_tag(js) + "\n" }
    end
    if custom_head_tags()[:css]
      custom_head_tags()[:css].each { |css| tags += stylesheet_link_tag(css) + "\n" }
    end
    tags
  end

  def link_to_textile_help    
    link_to "Syntax Reference", "/textile_help.html", :class => "textile-help", :title => "Syntax Reference",
            :onclick => 'window.open("/textile_help.html", "", "resizable=yes, location=no, width=500, height=640, menubar=no, status=no, scrollbars=yes"); return false;'
  end
  
  def field_with_help(form, field, textile = false)
    field
    html = ""
    hint = Hint.find_by_field("#{controller_name}-#{field.to_s}")
    if hint
      html = '<div class="help-container">'
      html += form.label field, field.to_s.capitalize.gsub(/_id$/, '').gsub(/_/, " "), :class => "help-link",
              :onmouseover => visual_effect(:appear, "#{controller_name}-#{field.to_s}", :duration => 0.2),
              :onmouseout => visual_effect(:fade, "#{controller_name}-#{field.to_s}", :duration => 0.2)
      html += link_to_textile_help if textile
      html += '<div class="help-box" id="' + "#{controller_name}-#{field.to_s}" +
              '" style="display:none;">' + hint.text + '</div>'
      html += '</div>'
    else
      html = form.label(field) + "<br/>"
    end
    html
  end

end
