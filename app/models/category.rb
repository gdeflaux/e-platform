class Category < ActiveRecord::Base
  has_many :newsitems
  has_many :documents
  has_many :service_providers
  
  validates_presence_of :name, :parent
  validates_uniqueness_of :name, :scope => :parent
  
  PARENTS = ["contact", "documents", "news", "providers"]
  
  def is_associated?
    if self.parent == "news"
      self.newsitems.count > 0
    elsif self.parent == "documents"
      self.documents.count > 0
    elsif self.parent == "providers"
      self.service_providers.count > 0  
    end
  end
  
  def update_permalink
    self.permalink = self.name.downcase.gsub(/[^\w]+/, '-').gsub(/(-)+$/, '')
  end
  
  def self.find_in_scope(permalink, c_scope)
    Category.find_by_permalink(permalink, :conditions => {:parent => c_scope})
  end
  
end
