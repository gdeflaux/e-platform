class Activity < ActiveRecord::Base
  belongs_to :axis
  has_many :activity_reports
  
  validates_uniqueness_of :name, :scope => :axis_id
  validates_presence_of :name
  
  acts_as_list :column => :sequence
  
  def scope_condition
    "axis_id = #{axis_id}"
  end
  
  def is_associated?
    self.activity_reports.count > 0
  end
  
  def self.order_list(axis_id)
    count = Activity.find_all_by_axis_id(axis_id, :conditions => {:active => true}).count
    return [["Top", 1]] if count <= 1
    return [["Top", 1], ["Bottom", 2]] if count == 2
    a = (2..count - 1).to_a.map { |x| [x,x] } << ["Bottom", count]
    a.insert(0, ["Top", 1])
    a
  end
  
  def self.children_activity
    Axis.children_axis.activities.first
  end
end
