class Language < ActiveRecord::Base
  has_many :assets
  
  validates_presence_of :name, :abbr
  validates_uniqueness_of :name, :abbr
    
  def self.sanitize_params(language)
    language[:name].capitalize!
    language[:abbr].upcase!
  end
  
  def sanitize!
    self.name.capitalize!
    self.abbr.upcase!
  end
  
  def is_associated?
    self.assets.count > 0
  end

end
