class Newsitem < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  has_many :assets, :as => :resource, :dependent => :destroy
  
  validates_presence_of :title, :lead, :category_id, :content
  validates_uniqueness_of :title
  
  validate :publihing_in_future
  
  def to_param
    self.permalink
  end
  
  private
  
  def publihing_in_future
    errors.add(:published_on, "date can't be in the future") if self.published_on > Time.now
  end
    
end