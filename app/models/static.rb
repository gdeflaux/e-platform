class Static < ActiveRecord::Base
  validates_presence_of :title, :content
  has_many :assets, :as => :resource, :dependent => :destroy
    
  def to_param
    self.permalink
  end
end
