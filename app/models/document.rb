class Document < ActiveRecord::Base
  belongs_to :category
  has_many :assets, :as => :resource, :dependent => :destroy
  
  validate_on_create :has_assets_on_create?
  validate_on_update :has_assets_on_update?
  validates_presence_of :title, :abstract, :publisher, :document_published_on, :category_id
  validates_uniqueness_of :title

  
  def to_param
    self.permalink
  end
  
  def self.latest
    Document.find(:all, :order => "created_at DESC", :limit => 3)
  end
  
  def self.highlighted
    Document.find_all_by_highlighted(true, :limit => "5", :order => "highlighted_on DESC")
  end
  
  def self.intervention_models
    cat = Category.find(:first, :conditions => {:name => "Intervention Models", :parent => "documents"})
    return [] if cat.nil?
    Document.find_all_by_category_id(cat.id)
  end
  
  def self.build_conditions_array(params)
    condition_string = Array.new
    placeholders = Hash.new

    if params[:search_title] != ""
      placeholders[:search_title] = "%" + params[:search_title].downcase + "%"
      condition_string << "(LOWER(documents.title) LIKE :search_title)"
    end

    if params[:search_publisher] != ""
      placeholders[:search_publisher] = "%" + params[:search_publisher].downcase + "%"
      condition_string << "(LOWER(documents.publisher) LIKE :search_publisher)"
    end
    
    if params[:search_published_on] != ""
      placeholders[:search_published_on] = params[:search_published_on]
      condition_string << "(documents.document_published_on = :search_published_on)"
    end
    
    if params[:search_category] != ""
      placeholders[:search_category] = params[:search_category]
      condition_string << "(documents.category_id = :search_category)"
    end
    
    [condition_string.join(" " + params[:search_operator]  + " "), placeholders]
  end
  
  private
  
  def has_assets_on_create?
    errors.add_to_base("You need to add at least one asset") if Asset.find_by_temporary_token(asset_temporary_token).nil?
  end
  
  def has_assets_on_update?
    if assets.count == 0
      errors.add_to_base("You need to add at least one asset")
    end
  end
  
end