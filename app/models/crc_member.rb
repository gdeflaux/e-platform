class CrcMember < ActiveRecord::Base
  has_many :activity_reports
  
  validates_uniqueness_of :name
  validates_presence_of :name
  
  def is_associated?
    self.activity_reports.count > 0
  end
end
