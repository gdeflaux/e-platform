class MonthlyReport < ActiveRecord::Base
  has_many :activity_reports, :dependent => :destroy
  accepts_nested_attributes_for :activity_reports
  
  validates_presence_of :period
  validates_uniqueness_of :period
  
  def find_activity_report(activity_id, member_id)
    if self.activity_reports.empty?
      return ActivityReport.new(:boys => 0, :girls => 0)
    else
      self.activity_reports.each do |r|
        return r if (r.activity_id == activity_id) and (r.crc_member_id == member_id)
      end
    end
  end
  
  def statistics(axes = nil, members = nil)
    if axes.nil?
      axes = Axis.find(:all, :order => "axes.sequence", :include => :activities, :conditions => "axes.id <> #{Axis.children_axis.id} AND activities.id IN (#{self.activity_list})")
    end
    if members.nil?
      members = CrcMember.find(:all, :order => "crc_members.name ASC", :conditions => "crc_members.id IN (" + self.member_list + ")")
    end

    stats = {
              :member_totals => {},
              :activity_totals => {},
              :gender_totals => {},
              :global_member_totals => {},
              :global_gender_totals => { :boys => 0, :girls => 0 },
              :children_totals => {},
              :children_gender_totals => { :boys => 0, :girls => 0 }
            }

    axes.each do |a|
      stats[:member_totals][a.id] = {}
      stats[:activity_totals][a.id] = {}
      stats[:gender_totals][a.id] = { :boys => 0, :girls => 0 }
    end

    members.each do |m|
      stats[:global_member_totals][m.id] = { :boys => 0, :girls => 0 }
      stats[:children_totals][m.id] = { :boys => 0, :girls => 0 }
    end
    
    children_axis_id = Axis.children_axis.id
    self.activity_reports.each do |ar|
      axis_id = ar.activity.axis_id
      next if axis_id == children_axis_id
      if stats[:member_totals][axis_id][ar.crc_member_id].nil?
        stats[:member_totals][axis_id][ar.crc_member_id] = {}
        stats[:member_totals][axis_id][ar.crc_member_id][:boys] = ar.boys
        stats[:member_totals][axis_id][ar.crc_member_id][:girls] = ar.girls
      else
        stats[:member_totals][axis_id][ar.crc_member_id][:boys] += ar.boys
        stats[:member_totals][axis_id][ar.crc_member_id][:girls] += ar.girls
      end
      
      if stats[:activity_totals][axis_id][ar.activity_id].nil?
        stats[:activity_totals][axis_id][ar.activity_id] = {}
        stats[:activity_totals][axis_id][ar.activity_id][:boys] = ar.boys
        stats[:activity_totals][axis_id][ar.activity_id][:girls] = ar.girls
      else
        stats[:activity_totals][axis_id][ar.activity_id][:boys] += ar.boys
        stats[:activity_totals][axis_id][ar.activity_id][:girls] += ar.girls
      end
      
      stats[:gender_totals][axis_id][:boys] += ar.boys
      stats[:gender_totals][axis_id][:girls] += ar.girls
      
      stats[:global_member_totals][ar.crc_member_id][:boys] += ar.boys
      stats[:global_member_totals][ar.crc_member_id][:girls] += ar.girls
      
      stats[:global_gender_totals][:boys] += ar.boys
      stats[:global_gender_totals][:girls] += ar.girls
    end
    
    children_ar = ActivityReport.find(:all, :conditions => "monthly_report_id = #{self.id} AND activity_id = #{Activity.children_activity.id}")
    children_ar.each do |ar|
      stats[:children_totals][ar.crc_member_id][:boys] = ar.boys
      stats[:children_totals][ar.crc_member_id][:girls] = ar.girls
      stats[:children_gender_totals][:boys] += ar.boys
      stats[:children_gender_totals][:girls] += ar.girls
    end
    
    stats 
  end
  
end
