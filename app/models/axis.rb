class Axis < ActiveRecord::Base
  has_many :activities, :dependent => :destroy, :order => "sequence ASC"
  accepts_nested_attributes_for :activities
  
  validates_uniqueness_of :name
  validates_presence_of :name, :desc
  
  acts_as_list :column => :sequence
  
  def is_associated?
    self.activities.count > 0
  end
  
  def self.order_list
    count = Axis.find(:all, :conditions => {:active => true}).count
    return [["Top", 2]] if count <= 2
    return [["Top", 2], ["Bottom", 3]] if count == 3
    a = (3..count - 1).to_a.map { |x| [x,x] } << ["Bottom", count]
    a.insert(0, ["Top", 2])
    a
  end
  
  def self.children_axis
    Axis.find_by_is_children_axis(true)
  end
  
  def self.axis_for_activities
    children_axis = Axis.children_axis
    Axis.find(:all, :conditions => "axes.id <> " + children_axis.id.to_s)
  end
      
  
  def active_activities
    Activity.find_all_by_axis_id(self.id, :order => :sequence, :conditions => {:active => true})
  end
  
  def inactive_activities
    Activity.find_all_by_axis_id(self.id, :order => :sequence, :conditions => {:active => false})
  end
    
end
