class ActivityReport < ActiveRecord::Base
  belongs_to :activity
  belongs_to :crc_member
  belongs_to :monthly_report
  
  validates_presence_of :boys, :girls, :activity_id, :crc_member_id
  validates_numericality_of :boys, :girls
end
