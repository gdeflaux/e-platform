class Chart < ActiveRecord::Base

  validates_presence_of :gender, :axes, :shape
  
  GENDER = %w{all by_gender}
  AXES = %w{all by_axis}
  SHAPE = %w{columns pie}
  
  def to_xml(data, members, axis)
    series = self.extract_series(data, members, axis)
    
    chart_class = case self.shape
      when "columns" then Ziya::Charts::Column
      when "pie" then Ziya::Charts::PieThreed
    end

    chart = chart_class.new
    chart.add(:theme, "pimp")
    chart.add(:axis_category_text, members.map { |m| m.name })
    if self.gender == "all"
      chart.add(:series, "Boys and girls", series)
    else
      series.each do |k,v|
        chart.add(:series, k.to_s.capitalize, v)
      end
    end
    chart.to_xml
  end
  
  def title(axis = nil)
    title = ""
    
    if self.axes == "all"
      if self.gender == "all"
        title = "Members overall participation"
      else
        title = "Members overall participation by gender"
      end
    else
      axis_name = Axis.find(axis).name
      if self.gender == "all"
        title = "Members participation on axis <em>#{axis_name}</em>"
      else
        title = "Members participation on axis <em>#{axis_name}</em> by gender"
      end
    end
    
    title += " (%)" if self.shape == "pie"
    title
  end
  
  protected
  
  def extract_series(data, members, axis)
    data_set = case self.axes
      when "all" then data[:global_member_totals]
      when "by_axis" then data[:member_totals][axis.to_i]
    end
    
    res = case self.gender
      when "all" then
        members.map do |m|
          sum = data_set[m.id][:boys] + data_set[m.id][:girls]
          {:value => sum}
        end
      when "by_gender" then
        series = {}
        series[:boys] = members.map { |m| {:value => data_set[m.id][:boys]} }
        series[:girls] = members.map { |m| {:value => data_set[m.id][:girls]} }
        series
    end
    res
  end
  
end
