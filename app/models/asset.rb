class Asset < ActiveRecord::Base
  RESOURCES_WITH_LANGUAGE = %w{Document}

  has_attached_file :data, {:styles => { :thumb => ["100x100#", :png], :normal => ["250x250>", :png],
                                        :normalized => ["640x480>", :png] }}.merge(PAPERCLIP_STORAGE_OPTIONS)
    
  validates_attachment_presence :data
  validates_attachment_content_type :data, :content_type => ['image/jpeg', 'image/png', 'application/pdf',
                                                              'application/msword', 'application/vnd.ms-powerpoint',
                                                              'application/vnd.ms-excel', 'audio/mp3']
  before_post_process :thumbnailable?
  
  belongs_to :user
  
  belongs_to :language
  
  validates_presence_of :title
  validates_presence_of :language, :if => Proc.new { |a| Asset::RESOURCES_WITH_LANGUAGE.include?(a.resource_type) }
  
  belongs_to :resource, :polymorphic => true
  
  before_create :clean_file_name
  
  def url(*args)
    data.url(*args)
  end
  
  def name
      data_file_name
  end

  def mime_type
    if data_content_type =~ /jpeg/
      "jpeg"
    elsif data_content_type =~ /png/
      "png"
    elsif data_content_type =~ /pdf/
      "pdf"
    elsif data_content_type =~ /msword/
      "doc"
    elsif data_content_type =~ /powerpoint/
      "ppt"
    elsif data_content_type =~ /excel/
      "xls"
    end
  end

  def notify_upload_success(page, token)
    page.hide "asset-upload-error"
    page.insert_html :top, "asset-list", :partial => 'shared/asset_list_item', :object => self, :locals => {:token => token}
    page.visual_effect :highlight, "asset_#{self.id}"
    
    page["asset-title"].value = nil
    page["asset-filename"].value = nil
    
    page["asset-upload-notice"].innerHTML = "Successfully added image"
    page.show "asset-upload-notice"
    page.visual_effect :fade, "asset-upload-notice", :duration => 3.0
  end
  
  def notify_upload_failure(page)
    page.hide "asset-upload-notice"
    
    page["asset-title"].value = nil
    page["asset-filename"].value = nil
    
    err_list_items = ""
    self.errors.each_full do |msg|
      err_list_items += "<li>#{msg}</li>"
    end
    page["asset-upload-error"].innerHTML = "<ul>#{err_list_items}</ul>"
    page.show "asset-upload-error"
  end

  def thumbnailable?
    !!(self.data_content_type =~ /(image|pdf)/)
  end
  
  def image_file?
    !!(self.data_content_type =~ /image/)
  end
  
  def audio_file?
    !!(self.data_content_type =~ /audio\/mp3/)
  end

  private

    def clean_file_name
      extension = File.extname(data_file_name)
      basename = File.basename(data_file_name, extension).gsub(/[^\w._-]+/, "_")
      self.data.instance_write(:file_name, "#{basename}#{extension.downcase}")
    end    
end
