class ServiceProvider < ActiveRecord::Base
  belongs_to :category
  has_and_belongs_to_many :services
  
  validates_presence_of :name, :description, :address, :category_id
  validates_uniqueness_of :name
  validates_format_of :url, :with => /^http:\/\//, :message => "must start with http://", :allow_blank => true
  
  def to_param
    self.permalink
  end
  
  def self.build_conditions_array(params)
    condition_string = Array.new
    placeholders = Hash.new
    
    if params[:search_name] != ""
      placeholders[:search_name] = "%" + params[:search_name].downcase + "%"
      condition_string << "(LOWER(service_providers.name) LIKE :search_name)"
    end

    if params[:search_contact_person] != ""
      placeholders[:search_contact_person] = "%" + params[:search_contact_person].downcase + "%"
      condition_string << "(LOWER(service_providers.contact_person) LIKE :search_contact_person)"
    end

    if params[:search_address] != ""
      placeholders[:search_address] = "%" + params[:search_address].downcase + "%"
      condition_string << "(LOWER(service_providers.address) LIKE :search_address)"
    end
    
    if params[:search_services]
      join = []
      where = []
      params[:search_services].each do |s|
        join <<  "INNER JOIN service_providers_services AS sps#{s} ON service_providers.id = sps#{s}.service_provider_id"
        where << "sps#{s}.service_id = #{s}"
      end
      distinct = params[:search_operator] == "OR" ? "DISTINCT" : ""
      sql = "SELECT #{distinct} service_providers.id FROM service_providers " + join.join(" \n") + " WHERE " + where.join(" " + params[:search_operator] + " ")
      sp = ServiceProvider.find_by_sql(sql).map { |s| s.id }
      placeholders[:search_services] = sp
      condition_string << "(service_providers.id IN (:search_services))"
    end
    
    if params[:search_category] != ""
      placeholders[:search_category] = params[:search_category]
      condition_string << "(service_providers.category_id = :search_category)"
    end
    
    [condition_string.join(" " + params[:search_operator]  + " "), placeholders]
  end
end
