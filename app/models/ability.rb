class Ability
  include CanCan::Ability
  
  def initialize (user)
    user ||= User.new # Guest user
    
    # Read permission for a specific action
    alias_action :reports_public_index, :reports_public_show, :show, :to => :read_reports
    
    # Read permissions for visitors
    can :read, Newsitem
    can :read, Static
    can :read, Partner
    can :read, Document
    can :read, ServiceProvider
    can :read, Link
    can :read_reports, MonthlyReport
    can :read, Chart
    
    if user.role == "super_administrator"
      can :manage, :all
    end
    
    # All but users
    if user.role == "administrator"
      # Back end
      can :manage, Category
      can :manage, Configuration
      can :manage, Language
      can :manage, Service
      can :read, User
      can :edit_self, User do |u|
        u.id == user.id
      end
      # Front end    
      can :manage, Asset
      can :manage, Document
      can :manage, Link
      can :manage, Newsitem
      can :manage, Partner
      can :manage, ServiceProvider
      can :manage, Static
      # Reporting
      can :manage, MonthlyReport
      can :manage, Chart
      can :manage, Axis
      can :manage, CrcMember
      can :manage, Activity
    end
    
    if user.role == "database_manager"
      can :manage, MonthlyReport
      can :manage, Axis
      can :manage, CrcMember
      can :manage, Activity
      can :edit_self, User do |u|
        u.id == user.id
      end
    end

  end
end