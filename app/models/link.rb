class Link < ActiveRecord::Base
  validates_presence_of :title, :url, :description
  validates_format_of :url, :with => /^http:\/\//, :message => "must start with http://"
  acts_as_list :column => :sequence
  
  def self.order_list
    count = Link.all.count
    return [["Top", 1]] if count <= 1
    return [["Top", 1], ["Bottom", 2]] if count == 2
    a = (2..count - 1).to_a.map { |x| [x,x] } << ["Bottom", count]
    a.insert(0, ["Top", 1])
    a
  end
  
end
