class User < ActiveRecord::Base
  acts_as_authentic
  
  validates_uniqueness_of :email, :username
  
  ROLES = %w[administrator database_manager]
  
  def self.generate_random_password
    (0...8).map{65.+(rand(25)).chr}.join
  end
  
  def is_super_admin?
    self.role == "super_administrator"
  end
  
end
