class Service < ActiveRecord::Base
  has_and_belongs_to_many :service_providers
  
  validates_presence_of :name
  validates_uniqueness_of :name
end
