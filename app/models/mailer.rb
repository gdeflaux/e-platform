class Mailer < ActionMailer::Base

  def contact(message)
    subject    '[E-Platform] Contact Form'
    recipients Configuration.find(:first).contact_email
    from       'e-platform@heroku.com'
    sent_on    Time.now
    
    body       :message => message
  end

  def request_password(message)
    subject     "[E-Platform] Password request"
    recipients  message[:email]
    from        "e-platform@heroku.com"
    sent_on     Time.now
    
    body        :message => message
  end
  
  def send_error_report(message)
    conf = Configuration.find(:first)
    technical_email = conf.technical_email.nil? ? conf.contact_email : conf.technical_email
    
    subject     "[E-Platform] Error report"
    recipients  technical_email
    from        "e-platform@heroku.com"
    sent_on     Time.now
    
    body        :message => message
  end
end
