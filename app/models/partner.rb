class Partner < ActiveRecord::Base
  validates_presence_of :name, :description, :in_crc_since, :lead
  validates_format_of :url, :with => /^http:\/\//, :message => "must start with http://", :allow_blank => true
  
  validates_uniqueness_of :name
  
  has_many :assets, :as => :resource, :dependent => :destroy
  
  def self.random_one
    partners = Partner.all
    if partners
      partners[rand(partners.size)]
    else
      nil
    end
  end
  
  def to_param
    self.permalink
  end
  
end
