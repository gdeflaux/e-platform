class PartnersController < ApplicationController

  include RedclothHooks

  authorize_resource
  
  after_filter :associate_assets_to_partner, :only => [:create]
  before_filter :destroy_unassociated_assets, :except => [:create, :destroy_asset, :upload_asset]
  # GET /partners
  # GET /partners.xml
  def index
    @partners = Partner.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @partners }
    end
  end

  # GET /partners/1
  # GET /partners/1.xml
  def show
    @partner = Partner.find_by_permalink!(params[:id])

    @partner.description = redcloth_image_replace(@partner.description, @partner.assets)
    @partner.description = RedCloth.new(@partner.description).to_html
     
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @partner }
    end
  end

  # GET /partners/new
  # GET /partners/new.xml
  def new
    @partner = Partner.new
    @partner.asset_temporary_token = generate_token
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @partner }
    end
  end

  # GET /partners/1/edit
  def edit
    @partner = Partner.find_by_permalink!(params[:id])
  end

  # POST /partners
  # POST /partners.xml
  def create
    @partner = Partner.new(params[:partner])
    @partner.permalink = @partner.name.downcase.gsub(/[^\w]+/, '-').gsub(/(-)+$/, '')

    respond_to do |format|
      if @partner.save
        flash[:notice] = 'Partner was successfully created.'
        format.html { redirect_to(@partner) }
        format.xml  { render :xml => @partner, :status => :created, :location => @partner }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @partner.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /partners/1
  # PUT /partners/1.xml
  def update
    @partner = Partner.find_by_permalink!(params[:id])

    respond_to do |format|
      if @partner.update_attributes(params[:partner])
        flash[:notice] = 'Partner was successfully updated.'
        format.html { redirect_to(@partner) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @partner.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /partners/1
  # DELETE /partners/1.xml
  def destroy
    @partner = Partner.find_by_permalink!(params[:id])
    @partner.destroy

    respond_to do |format|
      format.html { redirect_to(partners_url) }
      format.xml  { head :ok }
    end
  end

  def upload_asset
    @asset = Asset.new(params[:asset_form])
    @asset.resource_type = Partner.class_name
    
    if params[:id]    
      @partner = Partner.find(params[:id])
      @asset.resource_id = @partner.id
    end
    
    respond_to do |format|
      if @asset.save
        format.js do
          responds_to_parent do
            render :update do |page|
              @asset.notify_upload_success(page,  params[:asset_form][:temporary_token])
            end
          end
        end
      else
        format.js do
          responds_to_parent do
            render :update do |page|
              @asset.notify_upload_failure(page)
            end
          end
        end
      end
    end
  end
  
  def destroy_asset
    @asset = Asset.find(params[:asset_id])
    @asset.destroy
  end

private
  
  def associate_assets_to_partner
    @unassociated_assets = Asset.find(:all, :conditions => {:temporary_token => @partner.asset_temporary_token})
    @unassociated_assets.each do |asset|
      asset.resource_id = @partner.id
      asset.save
    end
  end

end
