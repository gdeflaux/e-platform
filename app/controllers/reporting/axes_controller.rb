class Reporting::AxesController < ApplicationController

  authorize_resource
  
  # GET /axes
  # GET /axes.xml
  def index
    @axes = Axis.find(:all, :order => :sequence)

    @active_axes = Array.new
    @inactive_axes = Array.new
    
    @axes.each do |u|
      if u.active
        @active_axes << u
      else
        @inactive_axes << u
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @axes }
    end
  end

  # GET /axes/1
  # GET /axes/1.xml
  def show
    @axis = Axis.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @axis }
    end
  end

  # GET /axes/new
  # GET /axes/new.xml
  def new
    @axis = Axis.new
    @axis.activities << Activity.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @axis }
    end
  end

  # GET /axes/1/edit
  def edit
    @axis = Axis.find(params[:id])
  end

  # POST /axes
  # POST /axes.xml
  def create
    @axis = Axis.new(params[:axis])
    @axis.is_children_axis = (Axis.count == 0) ? true : false
    @axis.active = true
    @axis.activities[0].active = true
    
    respond_to do |format|
      if @axis.save
        flash[:notice] = 'Axis was successfully created.'
        format.html { redirect_to reporting_axes_url }
      else
        format.html { render :action => "new" }
      end
    end
  end

  # PUT /axes/1
  # PUT /axes/1.xml
  def update
    @axis = Axis.find(params[:id])
    @axis.insert_at(params[:axis][:sequence])

    respond_to do |format|
      if @axis.update_attributes(params[:axis])
        flash[:notice] = 'Axis was successfully updated.'
        format.html { redirect_to reporting_axes_url }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  # DELETE /axes/1
  # DELETE /axes/1.xml
  def destroy
    @axis = Axis.find(params[:id])
    if @axis.is_children_axis
      flash[:error] = "Cannot delete chilren number axis"
    elsif @axis.is_associated?
      flash[:error] = "Cannot delete axis because still associated some activity"
    else
      @axis.destroy
    end
    
    respond_to do |format|
      format.html { redirect_to reporting_axes_url }
      format.xml  { head :ok }
    end
  end
  
  def activate
    @axis = Axis.find(params[:id])
    @axis.active = true
    @axis.insert_at(2)
    if @axis.save
      flash[:notice] = "Successfully activated axis"
    else
      flash[:error] = "Unable to activate axis"
    end
    redirect_to reporting_axes_url
  end
  
  def deactivate
    @axis = Axis.find(params[:id], :include => :activities)
    if @axis.is_children_axis
      flash[:error] = "Cannot to deactivate axis to report children numbers"
    else
      @axis.active = false
      @axis.move_to_bottom
      @axis.activities.each { |a| a.active = false }
    
      if @axis.save
        flash[:notice] = "Successfully deactivated axis"
      else
        flash[:error] = "Unable to deactivate axis"
      end
    end
    redirect_to reporting_axes_url
  end
end
