class Reporting::ChartsController < ApplicationController

  authorize_resource # kinda useless since there is no route to access this controller

  def show
    chart = Chart.find(params[:id])
    monthly_report = MonthlyReport.find(params[:monthly_report], :include => :activity_reports)
    axes = Axis.find(:all, :order => "axes.sequence", :include => :activities, :conditions => "activities.id NOT IN (#{Activity.children_activity.id}) AND activities.id IN (#{monthly_report.activity_list})")
    members = CrcMember.find(:all, :order => "crc_members.name ASC", :conditions => "crc_members.id IN (" + monthly_report.member_list + ")")
    stats = monthly_report.statistics(axes, members)
    render :xml => chart.to_xml(stats, members, params[:axis])
  end
end
