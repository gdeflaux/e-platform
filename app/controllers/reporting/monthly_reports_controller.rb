class Reporting::MonthlyReportsController < ApplicationController
  
  authorize_resource
  
  helper Ziya::HtmlHelpers::Charts, Ziya::YamlHelpers::Charts

  # GET /monthly_reports
  # GET /monthly_reports.xml
  def index
    @monthly_reports = MonthlyReport.find(:all, :order => "period DESC")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @monthly_reports }
    end
  end

  def reports_public_index
    if (request.domain == "localhost")
      @monthly_reports = MonthlyReport.find(:all, :order => "strftime('%Y',period) DESC, strftime('%m', period) ASC")
    else
      @monthly_reports = MonthlyReport.find(:all, :order => "EXTRACT(YEAR FROM period) DESC, EXTRACT(MONTH FROM period) ASC")
    end
  end

  # GET /monthly_reports/1
  # GET /monthly_reports/1.xml
  def show
    if params[:year] and params[:month]
      month = (params[:month].to_i < 10 and params[:month].length < 2) ? "0#{params[:month]}" : params[:month]
      period = "#{params[:year]}-#{month}-01"
      @monthly_report = MonthlyReport.find_by_period!(period, :include => :activity_reports)
    else
      @monthly_report = MonthlyReport.find(params[:id], :include => :activity_reports)
    end
    @axes = Axis.find(:all, :order => "axes.sequence, activities.sequence", :include => :activities, :conditions => "activities.id NOT IN (#{Activity.children_activity.id}) AND activities.id IN (#{@monthly_report.activity_list})")
    @members = CrcMember.find(:all, :order => "crc_members.name ASC", :conditions => "crc_members.id IN (" + @monthly_report.member_list + ")")
    @stats = @monthly_report.statistics(@axes, @members)
    @charts = Chart.find(:all, :order => "sequence ASC")
    
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  def reports_public_show
    show
  end
  
  # GET /monthly_reports/new
  # GET /monthly_reports/new.xml
  def new
    @monthly_report = MonthlyReport.new
    
    @axes = Axis.find(:all, :include => :activities, :order => "axes.sequence, activities.sequence", :conditions => ["axes.active = :active_axes AND activities.active = :active_activities", {:active_axes => true, :active_activities => true}])
    @members = CrcMember.find(:all, :order => "crc_members.name ASC", :conditions => ["active = :active", {:active => true}])
    #@activities = Activity.find(:all, :order => "activities.sequence ASC", :include => :axis, :conditions => ["axes.active = :active_axes AND activities.active = :active_activities", {:active_axes => true, :active_activities => true}])
    @monthly_report.activity_reports = Array.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @monthly_report }
    end
  end

  # GET /monthly_reports/1/edit
  def edit
    @monthly_report = MonthlyReport.find(params[:id], :include => :activity_reports)
    @axes = Axis.find(:all, :order => "axes.sequence, activities.sequence", :include => :activities, :conditions => "activities.id IN (" + @monthly_report.activity_list + ")")
    @members = CrcMember.find(:all, :order => "crc_members.name ASC", :conditions => "crc_members.id IN (" + @monthly_report.member_list + ")")
  end

  # POST /monthly_reports
  # POST /monthly_reports.xml
  def create
    @monthly_report = MonthlyReport.new(params[:monthly_report])
    @activities = Activity.find(:all, :include => :axis, :conditions => ["axes.active = :active_axes AND activities.active = :active_activities", {:active_axes => true, :active_activities => true}])
    @members = CrcMember.find(:all, :select => "crc_members.id", :conditions => ["active = :active", {:active => true}])    

    # Stores the list of active members and activities at creation in order to simplify the contruction of the edit form
    @monthly_report.activity_list = @activities.map { |a| a.id }.join(",")
    @monthly_report.member_list = @members.map { |m| m.id }.join(",")

    respond_to do |format|
      if @monthly_report.save
        flash[:notice] = 'MonthlyReport was successfully created.'
        format.html { redirect_to reporting_monthly_reports_url }
      else
        @axes = Axis.find(:all, :order => "axes.sequence, activities.sequence", :include => :activities, :conditions => ["axes.active = :active_axes AND activities.active = :active_activities", {:active_axes => true, :active_activities => true}])
        @members = CrcMember.find(:all, :order => "crc_members.name ASC", :conditions => ["active = :active", {:active => true}])
        #@activities = Activity.find(:all, :include => :axis, :conditions => ["axes.active = :active_axes AND activities.active = :active_activities", {:active_axes => true, :active_activities => true}])
        format.html { render :action => "new" }
      end
    end
  end

  # PUT /monthly_reports/1
  # PUT /monthly_reports/1.xml
  def update
    @monthly_report = MonthlyReport.find(params[:id], :include => :activity_reports)

    respond_to do |format|
      if @monthly_report.update_attributes(params[:monthly_report])
        flash[:notice] = 'MonthlyReport was successfully updated.'
        format.html { redirect_to reporting_monthly_reports_url }
        format.xml  { head :ok }
      else
        @axes = Axis.find(:all, :order => "axes.sequence, activities.sequence", :include => :activities, :conditions => "activities.id IN (" + @monthly_report.activity_list + ")")
        @members = CrcMember.find(:all, :order => "crc_members.name ASC", :conditions => "crc_members.id IN (" + @monthly_report.member_list + ")")
        format.html {render :action => "edit"}
      end
    end
  end

  # DELETE /monthly_reports/1
  # DELETE /monthly_reports/1.xml
  def destroy
    @monthly_report = MonthlyReport.find(params[:id])
    @monthly_report.destroy

    respond_to do |format|
      format.html { redirect_to reporting_monthly_reports_url }
      format.xml  { head :ok }
    end
  end
end
