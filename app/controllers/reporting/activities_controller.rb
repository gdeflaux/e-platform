class Reporting::ActivitiesController < ApplicationController

  authorize_resource

  # GET /activities/new
  # GET /activities/new.xml
  def new
    @activity = Activity.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @activity }
    end
  end

  # GET /activities/1/edit
  def edit
    @activity = Activity.find(params[:id])
  end

  # POST /activities
  # POST /activities.xml
  def create
    @activity = Activity.new(params[:activity])
    @activity.active = true

    respond_to do |format|
      if @activity.save
        flash[:notice] = 'Activity was successfully created.'
        format.html { redirect_to reporting_axes_url }
      else
        format.html { render :action => "new" }
      end
    end
  end

  # PUT /activities/1
  # PUT /activities/1.xml
  def update
    @activity = Activity.find(params[:id])
    @activity.insert_at(params[:activity][:sequence])
    
    respond_to do |format|
      if @activity.update_attributes(params[:activity])
        flash[:notice] = 'Activity was successfully updated.'
        format.html { redirect_to reporting_axes_url }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  # DELETE /activities/1
  # DELETE /activities/1.xml
  def destroy
    @activity = Activity.find(params[:id])
    
    if @activity.axis_id == Axis.children_axis.id
      flash[:error] = "Cannot delete activity of the children number axis"
    elsif @activity.is_associated?
      flash[:error] = "Cannot delete activity because used in a monthly report"
    else
      @activity.move_to_bottom if @activity.active
      @activity.destroy
    end

    respond_to do |format|
      format.html { redirect_to reporting_axes_url }
    end
  end
  
  def activate
    @activity = Activity.find(params[:id])
    if @activity.axis.active == true
      @activity.active = true
      if @activity.save
        @activity.move_to_top
        flash[:notice] = "Successfully activated activity"
      else
        flash[:error] = "Unable to activate activity"
      end
    else
      flash[:error] = "Cannot activate activity which axis is inactive"
    end
    redirect_to reporting_axes_url
  end
  
  def deactivate
    @activity = Activity.find(params[:id])
    if @activity.axis_id == Axis.children_axis.id
      flash[:error] = "Unable to deactivate activity of the children number axis"
    else
      @activity.active = false
      @activity.move_to_bottom
      if @activity.save
        flash[:notice] = "Successfully deactivated activity"
      else
        flash[:error] = "Unable to deactivate activity"
      end
    end
    redirect_to reporting_axes_url
  end
  
  def sequence_list
    @order_list = Activity.order_list(params[:activity_axis_id])
    respond_to do |format|
      format.js
    end
  end
  
end
