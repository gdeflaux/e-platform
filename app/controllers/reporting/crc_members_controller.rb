class Reporting::CrcMembersController < ApplicationController
  
  authorize_resource

  # GET /crc_members
  # GET /crc_members.xml
  def index
    @crc_members = CrcMember.all
    @active_members = Array.new
    @inactive_members = Array.new
    
    @crc_members.each do |u|
      if u.active
        @active_members << u
      else
        @inactive_members << u
      end
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @crc_members }
    end
  end

  # GET /crc_members/1
  # GET /crc_members/1.xml
  def show
    @crc_member = CrcMember.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @crc_member }
    end
  end

  # GET /crc_members/new
  # GET /crc_members/new.xml
  def new
    @crc_member = CrcMember.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @crc_member }
    end
  end

  # GET /crc_members/1/edit
  def edit
    @crc_member = CrcMember.find(params[:id])
  end

  # POST /crc_members
  # POST /crc_members.xml
  def create
    @crc_member = CrcMember.new(params[:crc_member])
    @crc_member.active = true
    
    respond_to do |format|
      if @crc_member.save
        flash[:notice] = 'CrcMember was successfully created.'
        format.html { redirect_to reporting_crc_members_url }
      else
        format.html { render :action => "new" }
      end
    end
  end

  # PUT /crc_members/1
  # PUT /crc_members/1.xml
  def update
    @crc_member = CrcMember.find(params[:id])

    respond_to do |format|
      if @crc_member.update_attributes(params[:crc_member])
        flash[:notice] = 'CrcMember was successfully updated.'
        format.html { redirect_to reporting_crc_members_url }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  # DELETE /crc_members/1
  # DELETE /crc_members/1.xml
  def destroy
    @crc_member = CrcMember.find(params[:id])
    
    if @crc_member.is_associated?
      flash[:error] = "Cannot delete member because used in a monthly report"
    else
      @crc_member.destroy
    end

    respond_to do |format|
      format.html { redirect_to reporting_crc_members_url }
      format.xml  { head :ok }
    end
  end
  
  def activate
    @member = CrcMember.find(params[:id])
    @member.active = true
    if @member.save
      flash[:notice] = "Successfully activated member"
    else
      flash[:error] = "Unable to activate member"
    end
    redirect_to reporting_crc_members_url
  end
  
  def deactivate
    @member = CrcMember.find(params[:id])
    @member.active = false
    if @member.save
      flash[:notice] = "Successfully deactivated member"
    else
      flash[:error] = "Unable to deactivate member"
    end
    redirect_to reporting_crc_members_url
  end
end
