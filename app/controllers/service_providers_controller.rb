class ServiceProvidersController < ApplicationController

  authorize_resource

  # GET /service_providers
  # GET /service_providers.xml
  def index
    if params[:category]
      cat = Category.find_in_scope(params[:category], "providers")
      @service_providers = ServiceProvider.paginate(:per_page => 10, :page => params[:page], :conditions => {:category_id => cat}, :joins => :category, :select => "service_providers.*, categories.name AS category_name")
      @title = "Service Providers - " + cat.name
    elsif params[:search_button]
      @service_providers = ServiceProvider.paginate(:per_page => 10, :page => params[:page], :conditions => ServiceProvider.build_conditions_array(params), :joins => :category, :select => "service_providers.*, categories.name AS category_name")
      @title = "Service Providers - Search results (#{@service_providers.total_entries})"
    else
      @service_providers = ServiceProvider.paginate(:per_page => 10, :page => params[:page], :joins => :category, :select => "service_providers.*, categories.name AS category_name")
      @title = "Service Providers"
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @service_providers }
    end
  end

  # GET /service_providers/new
  # GET /service_providers/new.xml
  def new
    @service_provider = ServiceProvider.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @service_provider }
    end
  end

  # GET /service_providers/1/edit
  def edit
    @service_provider = ServiceProvider.find_by_permalink!(params[:id])
  end

  # POST /service_providers
  # POST /service_providers.xml
  def create
    @service_provider = ServiceProvider.new(params[:service_provider])
    @service_provider.permalink = @service_provider.name.downcase.gsub(/[^\w]+/, '-').gsub(/(-)+$/, '')

    respond_to do |format|
      if @service_provider.save
        flash[:notice] = 'ServiceProvider was successfully created.'
        format.html { redirect_to(service_providers_path) }
        format.xml  { render :xml => @service_provider, :status => :created, :location => @service_provider }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @service_provider.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /service_providers/1
  # PUT /service_providers/1.xml
  def update
    params[:service_provider][:service_ids] ||= []
    @service_provider = ServiceProvider.find_by_permalink!(params[:id])

    respond_to do |format|
      if @service_provider.update_attributes(params[:service_provider])
        flash[:notice] = 'ServiceProvider was successfully updated.'
        format.html { redirect_to(service_providers_path) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @service_provider.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /service_providers/1
  # DELETE /service_providers/1.xml
  def destroy
    @service_provider = ServiceProvider.find_by_permalink!(params[:id])
    @service_provider.destroy

    respond_to do |format|
      format.html { redirect_to(service_providers_url) }
      format.xml  { head :ok }
    end
  end

end
