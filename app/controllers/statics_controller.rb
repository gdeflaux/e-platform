class StaticsController < ApplicationController
  
  include RedclothHooks
  
  after_filter :associate_assets_to_static, :only => [:create]
  before_filter :destroy_unassociated_assets, :except => [:create, :destroy_asset, :upload_asset]

  authorize_resource

  # GET /statics/1
  # GET /statics/1.xml
  def show
    @static = Static.find_by_permalink!(params[:id])
    
    @content = redcloth_image_replace(@static.content, @static.assets)
    @content = RedCloth.new(@content).to_html

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @static }
    end
end

  # GET /statics/1/edit
  def edit
    @static = Static.find_by_permalink!(params[:id])
  end

  # PUT /statics/1
  # PUT /statics/1.xml
  def update
    @static = Static.find_by_permalink!(params[:id])

    respond_to do |format|
      if @static.update_attributes(params[:static])
        flash[:notice] = 'Static was successfully updated.'
        format.html { redirect_to(@static) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @static.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /statics/1
  # DELETE /statics/1.xml
  def destroy
    @static = Static.find_by_permalink!(params[:id])
    @static.destroy

    respond_to do |format|
      format.html { redirect_to(statics_url) }
      format.xml  { head :ok }
    end
  end

  def upload_asset
    @asset = Asset.new(params[:asset_form])
    @asset.resource_type = Static.class_name
    
    if params[:id]
      @static = Static.find(params[:id])
      @asset.resource_id = @static.id
    end
    
    respond_to do |format|
      if @asset.save
        format.js do
          responds_to_parent do
            render :update do |page|
              @asset.notify_upload_success(page,  params[:asset_form][:temporary_token])
            end
          end
        end
      else
        format.js do
          responds_to_parent do
            render :update do |page|
              @asset.notify_upload_failure(page)           
            end
          end
        end
      end
    end
  end
  
  def destroy_asset
    @asset = Asset.find(params[:asset_id])
    @asset.destroy
  end

private

  def associate_assets_to_static
    @unassociated_assets = Asset.find(:all, :conditions => {:temporary_token => @static.asset_temporary_token})
    @unassociated_assets.each do |asset|
      asset.resource_id = @static.id
      asset.save
    end
  end

end
