class ContactUsController < ApplicationController

  def index
    if request.post?
      check_errors
      if @errors.empty?
        subject = Category.find(params[:subject])
        Mailer.deliver_contact(:name => params[:name], :email => params[:email], :subject => subject.name,
                                :message => params[:message])
        render :action => "confirmation"
      else
        render :action => "index"
      end
    end
  end
  
  def confirmation(name)
    # Useless method since action is never called as such - only using render(:action => "confirmation") - see Index method
  end

  private
  
  def check_errors()
    @errors = []
    @errors << "Please fill in your name" if params[:name].blank?
    if params[:email].blank?
      @errors << "Please fill in your email"
    else
      unless params[:email] =~ /^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/
        @errors << "Email address is invalid"
      end
    end
    @errors << "Select a subject" if params[:subject].blank?
    @errors << "Please enter a message" if params[:message].blank?  
  end

end
