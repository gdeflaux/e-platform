class AssetsController < ApplicationController

  authorize_resource

  in_place_edit_for :asset, :title

  def update
    @asset = Asset.find(params[:id])
    if @asset.update_attributes(params[:asset])
      render :text => @asset.language.name
    else
      render :text => @asset.language.name + " (Error)"
    end
  end
end
