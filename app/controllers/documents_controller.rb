class DocumentsController < ApplicationController
  
  after_filter :associate_assets_to_document, :only => [:create]
  before_filter :destroy_unassociated_assets, :except => [:create, :upload_asset, :destroy_asset]
  
  authorize_resource
  
  # GET /documents
  # GET /documents.xml
  def index
    if params[:category]
      cat = Category.find_in_scope(params[:category], "documents")
      @documents = Document.paginate(:per_page => 10, :page => params[:page], :order => "created_at DESC", :conditions => {:category_id => cat}, :joins => :category, :select => "documents.*, categories.name AS category_name")
      @title = "Documents - " + cat.name
    elsif params[:search_button]
      @documents = Document.paginate(:per_page => 10, :page => params[:page], :order => "created_at DESC", :conditions => Document.build_conditions_array(params), :joins => :category, :select => "documents.*, categories.name AS category_name") 
      @title = "Documents - Search results (#{@documents.total_entries})"
    else
      @documents = Document.paginate(:per_page => 10, :page => params[:page], :order => "created_at DESC", :joins => :category, :select => "documents.*, categories.name AS category_name")
      @title = "Documents"
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @documents }
      format.rss
    end
  end

  # GET /documents/1
  # GET /documents/1.xml
  def show
    @document = Document.find_by_permalink!(params[:id], :joins => :category, :select => "documents.*, categories.name AS category_name")

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @document }
    end
  end

  # GET /documents/new
  # GET /documents/new.xml
  def new
    @document = Document.new
    @document.asset_temporary_token = generate_token

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @document }
    end
  end

  # GET /documents/1/edit
  def edit
    @document = Document.find_by_permalink!(params[:id])
  end

  # POST /documents
  # POST /documents.xml
  def create
    @document = Document.new(params[:document])
    @document.permalink = @document.title.downcase.gsub(/[^\w]+/, '-').gsub(/(-)+$/, '')
    if params[:document][:highlighted] == "1"
      @document.highlighted_on = Date.today
    else
      @document.highlighted_on = nil
    end
    respond_to do |format|
      if @document.save
        flash[:notice] = 'Document was successfully created.'
        format.html { redirect_to(@document) }
        format.xml  { render :xml => @document, :status => :created, :location => @document }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @document.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /documents/1
  # PUT /documents/1.xml
  def update
    @document = Document.find_by_permalink!(params[:id])
    if params[:document][:highlighted] == "1"
      @document.highlighted_on = Date.today
    else
      @document.highlighted_on = nil
    end
    respond_to do |format|
      if @document.update_attributes(params[:document])
        flash[:notice] = 'Document was successfully updated.'
        format.html { redirect_to(@document) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @document.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.xml
  def destroy
    @document = Document.find_by_permalink!(params[:id])
    @document.destroy

    respond_to do |format|
      format.html { redirect_to(documents_url) }
      format.xml  { head :ok }
    end
  end
  
  def upload_asset
    @asset = Asset.new(params[:asset_form])
    @asset.resource_type = Document.class_name
    
    if params[:id]    
      @document = Document.find(params[:id])
      @asset.resource_id = @document.id
    end
    
    respond_to do |format|
      if @asset.save
        format.js do
          responds_to_parent do
            render :update do |page|
              @asset.notify_upload_success(page, params[:asset_form][:temporary_token])
            end
          end
        end
      else
        format.js do
          responds_to_parent do
            render :update do |page|
              @asset.notify_upload_failure(page)
            end
          end
        end
      end
    end
  end
  
  def destroy_asset
    @asset_count = Asset.find(:all, :conditions => {:temporary_token => params[:token]}).count
    if @asset_count > 1
      @asset = Asset.find(params[:asset_id])
      @asset.destroy
    end
  end
  
  private
  
  def associate_assets_to_document
    @unassociated_assets = Asset.find(:all, :conditions => {:temporary_token => @document.asset_temporary_token}) 
    @unassociated_assets.each do |asset|
      asset.resource_id = @document.id
      asset.save
    end
  end
  
end
