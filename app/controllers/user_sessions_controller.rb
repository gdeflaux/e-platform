class UserSessionsController < ApplicationController

  before_filter :set_page_title
  skip_filter :set_previous_url

  # GET /user_sessions/new
  # GET /user_sessions/new.xml
  def new
    if User.find(:all).count == 0 # No users in database
      redirect_to admin_new_admin_url
    else
      @user_session = UserSession.new

      respond_to do |format|
        format.html # new.html.erb
        format.xml  { render :xml => @user_session }
      end
    end
  end
  
  # POST /user_sessions
  # POST /user_sessions.xml
  def create
    @user_session = UserSession.new(params[:user_session])
    
    respond_to do |format|
      if @user_session.save
        flash[:notice] = 'Successfully logged in.'
        format.html { redirect_previous }
        format.xml  { render :xml => @user_session, :status => :created, :location => @user_session }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user_session.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /user_sessions/1
  # DELETE /user_sessions/1.xml
  def destroy
    @user_session = UserSession.find
    @user_session.destroy
    flash[:notice] = 'Successfully logged out.'
    
    respond_to do |format|
      format.html do
        if (session[:previous_url] =~ /#{root_path}admin(\/)?/) != nil or (session[:previous_url] =~ /(edit)|(new)$/) != nil
          redirect_to root_url
        else
          redirect_previous
        end
      end
      format.xml  { head :ok }
    end
  end
  
  private

    def set_page_title
      @page_title = "Login"
    end
end
