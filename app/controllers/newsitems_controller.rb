class NewsitemsController < ApplicationController

  include RedclothHooks
  
  after_filter :associate_assets_to_newsitem, :only => [:create]
  before_filter :destroy_unassociated_assets, :except => [:create, :destroy_asset, :upload_asset]
  
  authorize_resource

  # GET /newsitems
  # GET /newsitems.xml
  def index
    if params[:category]
      cat = Category.find_in_scope(params[:category], "news")
      @newsitems = Newsitem.paginate(:per_page => 10, :page => params[:page], :order => "published_on DESC", :conditions => {:category_id => cat}, :joins => :category, :select => "newsitems.*, categories.name AS category_name")
      @title = "Latest CRC News on " + cat.name
    else
      @newsitems = Newsitem.paginate(:per_page => 10, :page => params[:page], :order => "published_on DESC", :joins => :category, :select => "newsitems.*, categories.name AS category_name")
      @title = "Latest CRC News"
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @newsitems }
      format.rss
    end
  end

  # GET /newsitems/1
  # GET /newsitems/1.xml
  def show
    @newsitem = Newsitem.find_by_permalink!(params[:id], :joins => :category, :select => "newsitems.*, categories.name AS category_name")

    @newsitem.content = redcloth_image_replace(@newsitem.content, @newsitem.assets)
    @newsitem.content = RedCloth.new(@newsitem.content).to_html
    
    if @newsitem.nil?
      render(:action => "invalid_page")
    else
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @newsitem }
      end
    end
  end
  
  # GET /newsitems/new
  # GET /newsitems/new.xml
  def new
    @newsitem = Newsitem.new
    @newsitem.asset_temporary_token = generate_token
    
    respond_to do |format|
      format.html  #new.html.erb
      format.xml  { render :xml => @newsitem }
    end
  end

  # GET /newsitems/1/edit
  def edit
    @newsitem = Newsitem.find_by_permalink!(params[:id])
  end

  # POST /newsitems
  # POST /newsitems.xml
  def create
    @newsitem = Newsitem.new(params[:newsitem])
    @newsitem.permalink = @newsitem.title.downcase.gsub(/[^\w]+/, '-').gsub(/(-)+$/, '')
    @newsitem.user_id = current_user.id
    
    respond_to do |format|
      if @newsitem.save
        flash[:notice] = 'Newsitem was successfully created.'
        format.html { redirect_to(@newsitem) }
        format.xml  { render :xml => @newsitem, :status => :created, :location => @newsitem }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @newsitem.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /newsitems/1
  # PUT /newsitems/1.xml
  def update
    @newsitem = Newsitem.find_by_permalink!(params[:id])

    respond_to do |format|
      if @newsitem.update_attributes(params[:newsitem])
        flash[:notice] = 'Newsitem was successfully updated.'
        format.html { redirect_to(@newsitem) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @newsitem.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /newsitems/1
  # DELETE /newsitems/1.xml
  def destroy
    @newsitem = Newsitem.find_by_permalink!(params[:id])
    @newsitem.destroy

    respond_to do |format|
      format.html { redirect_to(newsitems_url) }
      format.xml  { head :ok }
    end
  end

  def upload_asset
    @asset = Asset.new(params[:asset_form])
    @asset.resource_type = Newsitem.class_name
    
    if params[:id]    
      @newsitem = Newsitem.find(params[:id])
      @asset.resource_id = @newsitem.id
    end
    
    respond_to do |format|
      if @asset.save
        format.js do
          responds_to_parent do
            render :update do |page|
              @asset.notify_upload_success(page,  params[:asset_form][:temporary_token])
            end
          end
        end
      else
        format.js do
          responds_to_parent do
            render :update do |page|
              @asset.notify_upload_failure(page)
            end
          end
        end
      end
    end
  end
  
  def destroy_asset
    @asset = Asset.find(params[:asset_id])
    @asset.destroy
  end
  
private
  
  def associate_assets_to_newsitem
    @unassociated_assets = Asset.find(:all, :conditions => {:temporary_token => @newsitem.asset_temporary_token})
    @unassociated_assets.each do |asset|
      asset.resource_id = @newsitem.id
      asset.save
    end
  end
  
end
