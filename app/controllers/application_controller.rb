# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.
 
require "RedCloth"

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  protect_from_forgery # See ActionController::RequestForgeryProtection for details
 
  helper_method :current_user#, :navigation_items, :navigation_items_admin
  
  # Scrub sensitive parameters from your log
  filter_parameter_logging :password
  
  after_filter :set_previous_url

  unless ActionController::Base.consider_all_requests_local
    rescue_from Exception,                            :with => :render_error
    rescue_from ActiveRecord::RecordNotFound,         :with => :render_not_found
    rescue_from ActionController::RoutingError,       :with => :render_not_found
    rescue_from ActionController::UnknownController,  :with => :render_not_found
    rescue_from ActionController::UnknownAction,      :with => :render_not_found
  end

  # Catch CanCan exception
  rescue_from CanCan::AccessDenied do |exception|
    if (current_user.nil?)
      flash[:error] = "You must be logged in!"
      redirect_away root_url
    else
      flash[:error] = "You are not allowed to perform this action!"
      redirect_previous
    end
  end
    
  def redirect_away(*params)
    session[:original_url] = request.url
    redirect_to *params
  end
  
  def redirect_back(*params)
    url = session[:original_url]
    session[:original_url] = nil
    if url
        redirect_to url
    else
      redirect_to *params
    end
  end
  
  def redirect_previous
    redirect_to(session[:previous_url])
  end

  private

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end
  
  def generate_token
    ActiveSupport::SecureRandom.hex(64).first(12)
  end
  
  def destroy_unassociated_assets
    if !current_user.nil? # in case no user is logged in (visitor)
      @assets = Asset.find(:all, :conditions => {:resource_id => nil, :user_id => current_user.id})
      if !@assets.empty?
        @assets.each { |asset| asset.destroy }
      end
    end
  end
  
  def set_previous_url
    session[:previous_url] = request.url
  end

  def render_not_found(exception)
    log_error(exception)
    @page_title = "Error 404"
    render :template => "/error/404.html.erb", :layout => "error", :status => 404
  end

  def render_error(exception)
    Mailer.deliver_send_error_report(:error => "#{exception.class} (#{exception.message}):\n  " + clean_backtrace(exception).join("\n  "),
                                      :url => request.url)
    log_error(exception)
    @page_title = "Error 500"
    render :template => "/error/500.html.erb", :layout => "error", :status => 500
  end
  
end
