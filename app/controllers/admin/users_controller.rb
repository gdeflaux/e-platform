class Admin::UsersController < ApplicationController
  
  def index
    authorize! :read, User
    @users = User.all
    @active_users = Array.new
    @inactive_users = Array.new
    
    @users.each do |u|
      if u.active
        @active_users << u
      else
        @inactive_users << u
      end
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @users }
    end
  end

  def show
    @user = User.find(params[:id])
    authorize! :read, @user
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @user }
    end
  end

  def new
    @user = User.new
    authorize! :manage, @user

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user }
    end
  end

  def new_admin
    if User.find(:all).count > 0 
      flash[:error] = "You are not allowed to do perform this action"
      redirect_to root_url
    else
      @user = User.new
      @user.role = "super_administrator"

      respond_to do |format|
        format.html {render :action => :new_admin, :layout => "user_sessions"}
        format.xml  { render :xml => @user }
      end
    end
  end
  
  def edit
    @user = User.find(params[:id])
    if !(can?(:edit_self, @user) or can?(:manage, User))
      raise CanCan::AccessDenied.new("Not authorized!")
    end
    
    if cannot? :manage, User # edit_self / layout to user_sessions so admin menu does not appear
      render :action => :edit, :layout => "user_sessions"
    end
  end

  def create
    @user = User.new(params[:user])
    authorize! :manage, @user
    
    @user.active = true # New users are active by default
    
    respond_to do |format|
      if @user.save
        flash[:notice] = 'Registration successful.'
        format.html { redirect_to(admin_users_url) }
        format.xml  { render :xml => @user, :status => :created, :location => @user }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def create_admin
   if User.find(:all).count > 0 
      flash[:error] = "You are not allowed to do perform this action"
      redirect_to root_url
    else
      @user = User.new(params[:user])
      @user.role = "super_administrator"
      @user.active = true
    
      @conf = Configuration.new
      @conf.contact_email = @user.email
    
      if @user.save and @conf.save
        flash[:notice] = "Successfully created first admin user"
        redirect_to root_url
      else
        render :action => "new_admin"
      end
    end
  end

  def update
    @user = User.find(params[:id])
    if !(can?(:edit_self, @user) or can?(:manage, User))
      raise CanCan::AccessDenied.new("Not authorized!")
    end
    
    if cannot? :manage, User
      params[:user][:role] = @user.role
      params[:user][:active] = @user.active
    end
    
    if @user.is_super_admin? 
      @user.active = true # Super admin cannot be deactivated
      @user.role = "super_administrator" # Super admin is always admin
    end

    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = 'Successfully updated profile.'
        format.html do
          if can? :manage, User 
            redirect_to(admin_users_url)
          else
            redirect_to(root_url)
          end
        end
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def activate
    authorize! :manage, User
    @user = User.find(params[:id])
    @user.active = true
    if @user.save
      flash[:notice] = "Successfully activated user"
    else
      flash[:error] = "Unable to activate user"
    end
    redirect_to admin_users_path
  end
  
  def deactivate
    authorize! :manage, User
    @user = User.find(params[:id])
    if @user.is_super_admin?
      flash[:error] = "Cannot deactivate a super administrator"
    else
      @user.active = false
      if @user.save
        flash[:notice] = "Successfully deactivated user"
      else
        flash[:error] = "Unable to deactivate user"
      end
    end
    redirect_to admin_users_path
  end
  
  def request_password
    user = User.find_by_email(params[:email], :conditions => ["active = :active", {:active => true}])
    if user
      new_password = User.generate_random_password
      user.password = new_password
      user.password_confirmation = new_password
      if user.save
        Mailer.deliver_request_password(:name => user.username, :password => new_password, :email => user.email)
        flash[:notice] = "Password was sent to #{user.email}"
      else
        flash[:error] = "Error while chenging password"
      end
    else
      flash[:error] = "No user found with email '#{params[:email]}'"
    end
    redirect_to login_url
  end
  
end
