class Admin::LanguagesController < ApplicationController

  authorize_resource
  
  # GET /languages
  # GET /languages.xml
  def index
    @languages = Language.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @languages }
      format.json {render :json => @languages.map {|l| [l.id, l.name]}} # for in-place editing
    end
  end

  # GET /languages/new
  # GET /languages/new.xml
  def new
    @language = Language.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @language }
    end
  end

  # GET /languages/1/edit
  def edit
    @language = Language.find(params[:id])
  end

  # POST /languages
  # POST /languages.xml
  def create
    @language = Language.new(params[:language])
    @language.sanitize!

    respond_to do |format|
      if @language.save
        flash[:notice] = 'Language was successfully created.'
        format.html { redirect_to admin_languages_url }
        format.xml  { render :xml => @language, :status => :created, :location => @language }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @language.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /languages/1
  # PUT /languages/1.xml
  def update
    @language = Language.find(params[:id])
    Language.sanitize_params(params[:language])

    respond_to do |format|
      if @language.update_attributes(params[:language])
        flash[:notice] = 'Language was successfully updated.'
        format.html { redirect_to admin_languages_url }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @language.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /languages/1
  # DELETE /languages/1.xml
  def destroy
    @language = Language.find(params[:id])
    
    if @language.is_associated?
      flash[:error] = "Language associated to document"
    else
      @language.destroy
    end
    
    respond_to do |format|
      format.html { redirect_to(admin_languages_url) }
      format.xml  { head :ok }
    end
  end
end
