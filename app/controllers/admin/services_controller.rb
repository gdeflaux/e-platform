class Admin::ServicesController < ApplicationController
  
  authorize_resource
  
  def index
    @services = Service.find(:all)
  end
  
  def new
    @service = Service.new
  end
  
  def create
    @service = Service.new(params[:service])
    if @service.save
      flash[:notice] = 'Successfully created service.'
      redirect_to admin_services_url
    else
      render :action => :new
    end
  end
  
  def edit
    @service = Service.find(params[:id])
  end
  
  def update
    @service = Service.find(params[:id])
    
     if @service.update_attributes(params[:service])
        flash[:notice] = 'Successfully updated service.'
        redirect_to admin_services_url 
      else
        render :action => :edit        
      end
  end
  
  def destroy
    @service = Service.find(params[:id])
    
    if @service.service_providers.count != 0 
      flash[:error] = "Cannot delete service because still associated to service providers"
    else
      @service.destroy
    end
  
    redirect_to admin_services_url
  end

end
