class Admin::CategoriesController < ApplicationController

  authorize_resource

  def index
    @categories = Category.find(:all, :order => :parent)
  end

  def new
    @category = Category.new
  end
  
  def create
    @category = Category.new(params[:category])
    @category.update_permalink
    
    if @category.save
      flash[:notice] = "Successfully created category"
      redirect_to admin_categories_url
    else
      render :action => :new
    end
  end
  
  def edit
    @category = Category.find(params[:id])
  end
  
  def update
    @category = Category.find(params[:id])
    @category.update_permalink
    if @category.update_attributes(params[:category])
      flash[:notice] = "Successfully updated category"
      redirect_to admin_categories_url
    else
      render :action => :edit
    end
  end
  
  def destroy
    @category = Category.find(params[:id])
    if @category.is_associated?
      flash[:error] = "Category associated to content item"
    else
      @category.destroy
    end
    
    redirect_to admin_categories_url
  end
end