class Admin::ConfigurationsController < ApplicationController

  authorize_resource

  def index
    @conf = Configuration.find(:first)
  end

  def edit
    @conf = Configuration.find(:first)
  end
  
  def update
    @conf = Configuration.find(:first)
    if @conf.update_attributes(params[:configuration])
      flash[:notice] = "Successfully updated configuration"
      redirect_to admin_configurations_path
    else
      render :action => :edit
    end
  end

end
