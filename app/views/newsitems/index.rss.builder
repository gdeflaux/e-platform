xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Latest CRC news"
    xml.description "Lastest news about CRC activities in Afghanistan"
    xml.link newsitems_url(:rss)
    
    for newsitem in @newsitems
      xml.item do
        xml.title newsitem.title
        xml.description newsitem.lead
        xml.pubDate newsitem.created_at.to_s(:rfc822)
        xml.link newsitem_url(newsitem)
        xml.guid newsitem_url(newsitem)
      end
    end
  end
end