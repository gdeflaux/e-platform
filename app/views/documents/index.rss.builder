xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Latest CRC related documents"
    xml.description "Lastest documents relateed to the activities of the CRC in Afghanistan"
    xml.link documents_url(:rss)
    
    for document in @documents
      xml.item do
        xml.title document.title
        xml.description document.abstract
        xml.pubDate document.created_at.to_s(:rfc822)
        xml.link document_url(document)
        xml.guid document_url(document)
      end
    end
  end
end