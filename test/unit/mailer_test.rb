require 'test_helper'

class MailerTest < ActionMailer::TestCase
  test "contact" do
    @expected.subject = 'Mailer#contact'
    @expected.body    = read_fixture('contact')
    @expected.date    = Time.now

    assert_equal @expected.encoded, Mailer.create_contact(@expected.date).encoded
  end

end
