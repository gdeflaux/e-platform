require 'test_helper'

class AxesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:axes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create axis" do
    assert_difference('Axis.count') do
      post :create, :axis => { }
    end

    assert_redirected_to axis_path(assigns(:axis))
  end

  test "should show axis" do
    get :show, :id => axes(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => axes(:one).to_param
    assert_response :success
  end

  test "should update axis" do
    put :update, :id => axes(:one).to_param, :axis => { }
    assert_redirected_to axis_path(assigns(:axis))
  end

  test "should destroy axis" do
    assert_difference('Axis.count', -1) do
      delete :destroy, :id => axes(:one).to_param
    end

    assert_redirected_to axes_path
  end
end
