require 'test_helper'

class CrcMembersControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:crc_members)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create crc_member" do
    assert_difference('CrcMember.count') do
      post :create, :crc_member => { }
    end

    assert_redirected_to crc_member_path(assigns(:crc_member))
  end

  test "should show crc_member" do
    get :show, :id => crc_members(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => crc_members(:one).to_param
    assert_response :success
  end

  test "should update crc_member" do
    put :update, :id => crc_members(:one).to_param, :crc_member => { }
    assert_redirected_to crc_member_path(assigns(:crc_member))
  end

  test "should destroy crc_member" do
    assert_difference('CrcMember.count', -1) do
      delete :destroy, :id => crc_members(:one).to_param
    end

    assert_redirected_to crc_members_path
  end
end
