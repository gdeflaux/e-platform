require 'test_helper'

class MonthlyReportsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:monthly_reports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create monthly_report" do
    assert_difference('MonthlyReport.count') do
      post :create, :monthly_report => { }
    end

    assert_redirected_to monthly_report_path(assigns(:monthly_report))
  end

  test "should show monthly_report" do
    get :show, :id => monthly_reports(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => monthly_reports(:one).to_param
    assert_response :success
  end

  test "should update monthly_report" do
    put :update, :id => monthly_reports(:one).to_param, :monthly_report => { }
    assert_redirected_to monthly_report_path(assigns(:monthly_report))
  end

  test "should destroy monthly_report" do
    assert_difference('MonthlyReport.count', -1) do
      delete :destroy, :id => monthly_reports(:one).to_param
    end

    assert_redirected_to monthly_reports_path
  end
end
