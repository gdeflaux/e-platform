// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

function asset_language_in_place_collection_select (dom_id, asset_path)
{
	new Ajax.InPlaceCollectionEditor (
		dom_id,
		asset_path,
		{
			loadCollectionURL: 'http://localhost:3000/admin/languages.json',
			callback: function(form, value) {
		      	return 'value=' + encodeURIComponent(value) +
						'&asset[language_id]=' + encodeURIComponent(value) +
		             	'&_method=PUT'
					},
			ajaxoptions: {method: 'post'}
		}
	);
}