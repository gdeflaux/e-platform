class AddContactAndPhoneNumberToServiceProviders < ActiveRecord::Migration
  def self.up
    add_column :service_providers, :contact_person, :string
    add_column :service_providers, :phone_number, :string
  end

  def self.down
    remove_column :service_providers, :phone_number
    remove_column :service_providers, :contact_person
  end
end
