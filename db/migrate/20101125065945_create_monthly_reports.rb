class CreateMonthlyReports < ActiveRecord::Migration
  def self.up
    create_table :monthly_reports do |t|
      t.integer :month
      t.integer :year

      t.timestamps
    end
  end

  def self.down
    drop_table :monthly_reports
  end
end
