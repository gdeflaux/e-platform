class AddPermalinkToServiceProvider < ActiveRecord::Migration
  def self.up
    add_column :service_providers, :permalink, :string
  end

  def self.down
    remove_column :service_providers, :permalink
  end
end
