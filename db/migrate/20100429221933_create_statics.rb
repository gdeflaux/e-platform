class CreateStatics < ActiveRecord::Migration
  def self.up
    create_table :statics do |t|
      t.string :title, :permalink
      t.text :content
      t.integer :author_id

      t.timestamps
    end
  end

  def self.down
    drop_table :statics
  end
end
