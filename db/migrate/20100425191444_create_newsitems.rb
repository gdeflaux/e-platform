class CreateNewsitems < ActiveRecord::Migration
  def self.up
    create_table :newsitems do |t|
      t.string :title, :permalink
      t.text :lead, :content
      t.datetime :published_on
      t.integer :author_id
      t.timestamps
    end
  end

  def self.down
    drop_table :newsitems
  end
end
