class AddAssetTemporaryTokenToPartners < ActiveRecord::Migration
  def self.up
    add_column :partners, :asset_temporary_token, :string
  end

  def self.down
    remove_column :partners, :asset_temporary_token
  end
end
