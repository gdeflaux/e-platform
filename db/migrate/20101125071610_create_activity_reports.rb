class CreateActivityReports < ActiveRecord::Migration
  def self.up
    create_table :activity_reports do |t|
      t.integer :boys
      t.integer :girls
      t.integer :activity_id, :crc_member_id, :monthly_report_id

      t.timestamps
    end
  end

  def self.down
    drop_table :activity_reports
  end
end
