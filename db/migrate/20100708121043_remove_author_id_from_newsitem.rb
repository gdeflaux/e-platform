class RemoveAuthorIdFromNewsitem < ActiveRecord::Migration
  def self.up
    remove_column :newsitems, :author_id
  end

  def self.down
    add_column :newsitems, :author_id, :integer
  end
end
