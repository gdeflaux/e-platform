class AddLanguageToAssets < ActiveRecord::Migration
  def self.up
    add_column :assets, :language_id, :integer
  end

  def self.down
    remove_column :assets, :language_id
  end
end
