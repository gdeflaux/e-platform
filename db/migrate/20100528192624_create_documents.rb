class CreateDocuments < ActiveRecord::Migration
  def self.up
    create_table :documents do |t|
      t.string :title
      t.string :subtitle
      t.integer :document_published_on
      t.text :abstract
      t.integer :category_id
      t.string :publisher

      t.timestamps
    end
  end

  def self.down
    drop_table :documents
  end
end
