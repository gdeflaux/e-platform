class AddEmailToServiceProviders < ActiveRecord::Migration
  def self.up
    add_column :service_providers, :email, :string
  end

  def self.down
    remove_column :service_providers, :email
  end
end
