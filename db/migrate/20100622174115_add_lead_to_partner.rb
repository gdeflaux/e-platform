class AddLeadToPartner < ActiveRecord::Migration
  def self.up
    add_column :partners, :lead, :text
  end

  def self.down
    remove_column :partners, :lead
  end
end
