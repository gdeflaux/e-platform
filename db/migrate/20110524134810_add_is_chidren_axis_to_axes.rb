class AddIsChidrenAxisToAxes < ActiveRecord::Migration
  def self.up
    add_column :axes, :is_children_axis, :boolean
  end

  def self.down
    remove_column :axes, :is_children_axis
  end
end
