class AddAssetTemporaryTokenToNewsitem < ActiveRecord::Migration
  def self.up
    add_column :newsitems, :asset_temporary_token, :string
  end

  def self.down
    remove_column :newsitems, :asset_temporary_token
  end
end
