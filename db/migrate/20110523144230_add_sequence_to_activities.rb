class AddSequenceToActivities < ActiveRecord::Migration
  def self.up
    add_column :activities, :sequence, :integer
  end

  def self.down
    remove_column :activities, :sequence
  end
end
