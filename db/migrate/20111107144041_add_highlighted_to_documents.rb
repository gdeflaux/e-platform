class AddHighlightedToDocuments < ActiveRecord::Migration
  def self.up
    add_column :documents, :highlighted, :boolean
  end

  def self.down
    remove_column :documents, :highlighted
  end
end
