class AddListsToMonthlyReport < ActiveRecord::Migration
  def self.up
    add_column :monthly_reports, :member_list, :string
    add_column :monthly_reports, :activity_list, :string
  end

  def self.down
    remove_column :monthly_reports, :activity_list
    remove_column :monthly_reports, :member_list
  end
end
