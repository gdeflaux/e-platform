class AddUserIdToNewsitem < ActiveRecord::Migration
  def self.up
    add_column :newsitems, :user_id, :integer
  end

  def self.down
    remove_column :newsitems, :user_id
  end
end
