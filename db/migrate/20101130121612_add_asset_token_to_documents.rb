class AddAssetTokenToDocuments < ActiveRecord::Migration
  def self.up
    add_column :documents, :asset_temporary_token, :string
  end

  def self.down
    remove_column :documents, :asset_temporary_token
  end
end
