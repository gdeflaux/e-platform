class AddTechnicalEmailToConfigurations < ActiveRecord::Migration
  def self.up
    add_column :configurations, :technical_email, :string
  end

  def self.down
    remove_column :configurations, :technical_email
  end
end
