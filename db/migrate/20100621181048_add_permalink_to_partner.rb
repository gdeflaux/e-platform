class AddPermalinkToPartner < ActiveRecord::Migration
  def self.up
    add_column :partners, :permalink, :string
  end

  def self.down
    remove_column :partners, :permalink
  end
end
