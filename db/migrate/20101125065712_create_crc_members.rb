class CreateCrcMembers < ActiveRecord::Migration
  def self.up
    create_table :crc_members do |t|
      t.string :name
      t.boolean :active

      t.timestamps
    end
  end

  def self.down
    drop_table :crc_members
  end
end
