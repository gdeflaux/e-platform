class AddSequenceToLinks < ActiveRecord::Migration
  def self.up
    add_column :links, :sequence, :integer
  end

  def self.down
    remove_column :links, :sequence
  end
end
