class RemoveMonthFromMonthlyReport < ActiveRecord::Migration
  def self.up
    remove_column :monthly_reports, :month
    remove_column :monthly_reports, :year
    add_column :monthly_reports, :period, :date
  end

  def self.down
    add_column :monthly_reports, :month, :integer
    add_column :monthly_reports, :year, :integer
    remove_column :monthly_reports, :period
  end
end
