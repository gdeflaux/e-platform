class AddIndexes < ActiveRecord::Migration
  def self.up
    add_index :newsitems, :permalink
    add_index :newsitems, :user_id
    add_index :newsitems, :category_id
    
    add_index :statics, :permalink
    
    add_index :partners, :permalink
    
    add_index :documents, :permalink
    add_index :documents, :category_id
    
    add_index :service_providers, :permalink
    add_index :service_providers, :category_id
    
    add_index :service_providers_services, :service_provider_id
    add_index :service_providers_services, :service_id
    
    add_index :assets, [:resource_id, :resource_type]
  end

  def self.down
  end
end
