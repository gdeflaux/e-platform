class AddSequenceToAxes < ActiveRecord::Migration
  def self.up
    add_column :axes, :sequence, :integer
  end

  def self.down
    remove_column :axes, :sequence
  end
end
