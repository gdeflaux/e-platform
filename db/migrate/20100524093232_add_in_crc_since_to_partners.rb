class AddInCrcSinceToPartners < ActiveRecord::Migration
  def self.up
    add_column :partners, :in_crc_since, :integer
  end

  def self.down
    remove_column :partners, :in_crc_since
  end
end
