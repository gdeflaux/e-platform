class AddHighlightedOnToDocuments < ActiveRecord::Migration
  def self.up
    add_column :documents, :highlighted_on, :date
  end

  def self.down
    remove_column :documents, :highlighted_on
  end
end
