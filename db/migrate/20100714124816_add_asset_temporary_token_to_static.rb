class AddAssetTemporaryTokenToStatic < ActiveRecord::Migration
  def self.up
    add_column :statics, :asset_temporary_token, :string
  end

  def self.down
    remove_column :statics, :asset_temporary_token
  end
end
