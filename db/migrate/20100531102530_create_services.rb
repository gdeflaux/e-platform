class CreateServices < ActiveRecord::Migration
  def self.up
    create_table :services do |t|
      t.string :name

      t.timestamps
    end
    
    create_table :service_providers_services, :id => false do |t|
      t.integer :service_provider_id, :service_id
    end
  end

  def self.down
    drop_table :services
    drop_table :service_providers_services
  end
end
