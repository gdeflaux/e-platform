class CreateAxes < ActiveRecord::Migration
  def self.up
    create_table :axes do |t|
      t.string :name
      t.boolean :active

      t.timestamps
    end
  end

  def self.down
    drop_table :axes
  end
end
