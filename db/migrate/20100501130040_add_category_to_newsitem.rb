class AddCategoryToNewsitem < ActiveRecord::Migration
  def self.up
    add_column :newsitems, :category_id, :integer
  end

  def self.down
    remove_column :newsitems, :category_id
  end
end
