class AddPermalinkToDocument < ActiveRecord::Migration
  def self.up
    add_column :documents, :permalink, :string
  end

  def self.down
    remove_column :documents, :permalink
  end
end
