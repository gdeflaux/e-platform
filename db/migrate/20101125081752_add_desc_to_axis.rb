class AddDescToAxis < ActiveRecord::Migration
  def self.up
    add_column :axes, :desc, :string
  end

  def self.down
    remove_column :axes, :desc
  end
end
