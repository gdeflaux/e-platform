class AddTemporaryTokenToAsset < ActiveRecord::Migration
  def self.up
    add_column :assets, :temporary_token, :string
  end

  def self.down
    remove_column :assets, :temporary_token
  end
end
