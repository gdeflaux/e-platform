# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111117181923) do

  create_table "activities", :force => true do |t|
    t.string    "name"
    t.boolean   "active"
    t.integer   "axis_id"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.integer   "sequence"
  end

  create_table "activity_reports", :force => true do |t|
    t.integer   "boys"
    t.integer   "girls"
    t.integer   "activity_id"
    t.integer   "crc_member_id"
    t.integer   "monthly_report_id"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "assets", :force => true do |t|
    t.string    "title"
    t.integer   "resource_id"
    t.string    "resource_type"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "data_file_name"
    t.string    "data_content_type"
    t.integer   "data_file_size"
    t.timestamp "data_updated_at"
    t.string    "temporary_token"
    t.integer   "user_id"
    t.integer   "language_id"
  end

  add_index "assets", ["resource_id", "resource_type"], :name => "index_assets_on_resource_id_and_resource_type"

  create_table "axes", :force => true do |t|
    t.string    "name"
    t.boolean   "active"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "desc"
    t.integer   "sequence"
    t.boolean   "is_children_axis"
  end

  create_table "categories", :force => true do |t|
    t.string    "name"
    t.string    "parent"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "permalink"
  end

  create_table "charts", :force => true do |t|
    t.string    "gender"
    t.string    "axes"
    t.string    "shape"
    t.integer   "sequence"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "configurations", :force => true do |t|
    t.string    "contact_email"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "technical_email"
  end

  create_table "crc_members", :force => true do |t|
    t.string    "name"
    t.boolean   "active"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "documents", :force => true do |t|
    t.string    "title"
    t.string    "subtitle"
    t.integer   "document_published_on"
    t.text      "abstract"
    t.integer   "category_id"
    t.string    "publisher"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "permalink"
    t.string    "asset_temporary_token"
    t.boolean   "highlighted"
    t.date      "highlighted_on"
  end

  add_index "documents", ["category_id"], :name => "index_documents_on_category_id"
  add_index "documents", ["permalink"], :name => "index_documents_on_permalink"

  create_table "hints", :force => true do |t|
    t.string    "field"
    t.text      "text"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "languages", :force => true do |t|
    t.string    "name"
    t.string    "abbr"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "links", :force => true do |t|
    t.string    "title"
    t.string    "url"
    t.text      "description"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.integer   "sequence"
  end

  create_table "monthly_reports", :force => true do |t|
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.date      "period"
    t.string    "member_list"
    t.string    "activity_list"
  end

  create_table "newsitems", :force => true do |t|
    t.string    "title"
    t.string    "permalink"
    t.text      "lead"
    t.text      "content"
    t.timestamp "published_on"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.integer   "category_id"
    t.integer   "user_id"
    t.string    "asset_temporary_token"
  end

  add_index "newsitems", ["category_id"], :name => "index_newsitems_on_category_id"
  add_index "newsitems", ["permalink"], :name => "index_newsitems_on_permalink"
  add_index "newsitems", ["user_id"], :name => "index_newsitems_on_user_id"

  create_table "partners", :force => true do |t|
    t.string    "name"
    t.text      "description"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.integer   "in_crc_since"
    t.string    "url"
    t.string    "permalink"
    t.text      "lead"
    t.string    "asset_temporary_token"
  end

  add_index "partners", ["permalink"], :name => "index_partners_on_permalink"

  create_table "service_providers", :force => true do |t|
    t.string    "name"
    t.text      "description"
    t.string    "address"
    t.string    "url"
    t.integer   "category_id"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "permalink"
    t.string    "contact_person"
    t.string    "phone_number"
    t.string    "email"
  end

  add_index "service_providers", ["category_id"], :name => "index_service_providers_on_category_id"
  add_index "service_providers", ["permalink"], :name => "index_service_providers_on_permalink"

  create_table "service_providers_services", :id => false, :force => true do |t|
    t.integer "service_provider_id"
    t.integer "service_id"
  end

  add_index "service_providers_services", ["service_id"], :name => "index_service_providers_services_on_service_id"
  add_index "service_providers_services", ["service_provider_id"], :name => "index_service_providers_services_on_service_provider_id"

  create_table "services", :force => true do |t|
    t.string    "name"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "statics", :force => true do |t|
    t.string    "title"
    t.string    "permalink"
    t.text      "content"
    t.integer   "author_id"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "asset_temporary_token"
  end

  add_index "statics", ["permalink"], :name => "index_statics_on_permalink"

  create_table "user_sessions", :force => true do |t|
    t.string    "username"
    t.string    "password"
    t.timestamp "created_at"
    t.timestamp "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string    "username"
    t.string    "email"
    t.string    "crypted_password"
    t.string    "password_salt"
    t.string    "persistence_token"
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.string    "role"
    t.boolean   "active"
  end

end
