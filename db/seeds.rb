# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#   
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Major.create(:name => 'Daley', :city => cities.first)

# Categories for news
Category.create(:name => "Juvenile Justice", :parent => "news", :permalink => "juvenile-justice")
Category.create(:name => "Health", :parent => "news", :permalink => "health")
Category.create(:name => "Livelyhood", :parent => "news", :permalink => "livelyhood")
Category.create(:name => "Protection", :parent => "news", :permalink => "protection")

# Categories for documents
Category.create(:name => "Study", :parent => "documents", :permalink => "study")
Category.create(:name => "Report", :parent => "documents", :permalink => "report")

# Categories for service provides
Category.create(:name => "Hospital", :parent => "providers", :permalink => "hospital")
Category.create(:name => "Social Services", :parent => "providers", :permalink => "social-services")

# Categories for the contact form
Category.create(:name => "CRC Activities", :parent => "contact", :permalink => "crc-activities")

# About us page
Static.create(:title => "About CRC", :content => "Add content here", :permalink => "about-crc")

# Services
Service.create(:name => "Counseling")
Service.create(:name => "Day care")
Service.create(:name => "Vocational training")

# Languages
Language.create(:name => "English", :abbr => "EN")
Language.create(:name => "Dari", :abbr => "DR")
Language.create(:name => "Pashtu", :abbr => "PH")

# Charts
Chart.create(:gender => "all", :axes => "all", :shape => "columns", :sequence => 1)
Chart.create(:gender => "all", :axes => "all", :shape => "pie", :sequence => 2)
Chart.create(:gender => "by_gender", :axes => "all", :shape => "columns", :sequence => 3)
Chart.create(:gender => "all", :axes => "by_axis", :shape => "columns", :sequence => 4)
Chart.create(:gender => "all", :axes => "by_axis", :shape => "pie", :sequence => 5)
Chart.create(:gender => "by_gender", :axes => "by_axis", :shape => "columns", :sequence => 6)