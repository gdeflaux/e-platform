include ActionView::Helpers::UrlHelper 

module RedclothHooks
  
  def redcloth_image_replace(text, assets)
    asset_names = redcloth_get_asset_names(assets)
    player_counter = 0
    text.gsub(/(!([<>=]?)(.*)!)/) do |match|
      asset_position = $2
      asset_name = $3      
      if asset_names.include?(asset_name)
        asset = Asset.find(asset_names.index(asset_name))
        if asset.image_file?
          img_tag = "<img src=\"#{asset.url(:normal)}\" />"
          link_image = "<a href=\"#{asset.url(:normalized)}\" rel=\"lightbox-set\" title=\"#{asset.title}\">#{img_tag}</a>"
          redcloth_position_image(asset_position, link_image)
        elsif asset.audio_file?
          player_counter += 1
          host = request.protocol + request.host_with_port
          media_player_html(player_counter, asset.url(:original, false), asset.title, :audio, host)
        else
          "" # other file type not supported
        end
      elsif (asset_name =~ /http:\/\//) != nil # inline image
        img_tag = "<img src=\"#{asset_name}\" />"
        link_image = "<a href=\"#{asset_name}\" rel=\"lightbox-set\" title=\"\">#{img_tag}</a>"
        redcloth_position_image(asset_position, link_image)
      else
        "" # remove invalid references
      end
    end
  end
  
  private
  
  def redcloth_get_asset_names(assets)
    names = Array.new
    assets.each do |asset|
      names[asset.id] = asset.name
    end
    names
  end
    
  def redcloth_position_image(position, image)
      if position == ">"
        image = '<p class="img-right">' + image + "</p>"
      elsif position == "="
        image = '<p class="img-center">' + image + "</p>"
      else #if (position == "<") or (position == "")
        image = '<p class="img-left">' + image + "</p>"
      end
      image
  end
  
  def media_player_html(player_counter, filename, title, type, host)
    case type
    when :video
      height = 270
      width = 480
    else # :audio and others
      height = 24
      width = 480
    end
    link = link_to "[MP3]", filename
    "<div class=\"mediaplayer\"><div id=\"jwplayer#{player_counter}\">Loading the player ...</div></div>" +
    "<p class=\"media-title\">#{title} #{link}</p>" +
    "<script type=\"text/javascript\">jwplayer('jwplayer#{player_counter}\').setup({'flashplayer': '#{host}/jwplayer.swf', 'file': '#{filename}', 'height': #{height}, 'width': #{width}, 'controlbar': 'bottom'});</script>"
  end

end