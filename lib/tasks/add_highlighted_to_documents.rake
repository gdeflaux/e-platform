namespace :db do
  desc "Add highlighted to documents"
  task :add_highlighted_to_documents => :environment do        
    Hint.create(:field => "documents-highlighted", :text => "If checked, the document will appear in the Highlighted Document box on the right column")
  end
end