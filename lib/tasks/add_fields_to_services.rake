namespace :db do
  desc "Add fields to services"
  task :add_fields_to_services => :environment do        
    #service providers
    Hint.create(:field => "service_providers-email", :text => "Make sure the email is valid.")
    Hint.create(:field => "service_providers-phone_number", :text => "Phone number of the service provider.")
    Hint.create(:field => "service_providers-contact_person", :text => "Name of the contact person identified.")
    
    ServiceProvider.all.each do |sp|
      sp.email = ""
      sp.contact_person = ""
      sp.phone_number = ""
      sp.save
    end
  end
end