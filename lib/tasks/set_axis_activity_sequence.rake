namespace :db do
  desc "Sets the sequence for axis and activities"
  task :set_axis_activity_sequence => :environment do
    axis_sequence = 1
    Axis.find(:all, :order => "active DESC").each do |axis|
      activity_sequence = 1
      axis.sequence = axis_sequence
      Activity.find_all_by_axis_id(axis.id, :order => "active DESC").each do |activity|
        activity.sequence = activity_sequence
        activity.save
        activity_sequence += 1
      end
      axis.save
      axis_sequence += 1
    end
  end
end