namespace :db do
  desc "Adds the children axis"
  task :add_children_axis => :environment do
    # Sets is_children_axis to false for all existing axis
    axes = Axis.all
    axes.each do |axis|
      axis.is_children_axis = false
      axis.save
    end
    
    # Children axis
    caxis = Axis.new
    caxis.name = "Number of children"
    caxis.active = true
    caxis.desc = "Number of children"
    caxis.is_children_axis = true
    caxis.save
    caxis.insert_at(1)
    
    # Children activity
    cact = Activity.new
    cact.axis_id = caxis.id
    cact.name = "Number of children"
    cact.active = true
    cact.sequence = 1
    cact.save
    
    # Adds the children activity axis activity to the existing monthly reports
    reports = MonthlyReport.all
    members = CrcMember.all
    reports.each do |report|
      members.each do |member|
        ar = ActivityReport.new
        ar.activity_id = cact.id
        ar.crc_member_id = member.id
        ar.monthly_report_id = report.id
        ar.boys = 0
        ar.girls = 0
        ar.save
      end
      report.activity_list += ",#{cact.id}"
      report.save
    end
    
  end
end