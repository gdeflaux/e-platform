namespace :db do
  desc "Add assets to partners"
  task :add_assets_to_partners => :environment do        
    Hint.create(:field => "partners-description", :text => "Use textile syntax to format the text.")
  end
end