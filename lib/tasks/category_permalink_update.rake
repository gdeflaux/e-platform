namespace :db do
  desc "Updating categories permalinks according to categories names"
  task :update_categories_permalink => :environment do
    Category.all.each do |c|
      puts "#{c.parent} - #{c.name}"
      c.update_permalink
      c.save
    end
  end
end