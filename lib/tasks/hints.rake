namespace :db do
  desc "Seeds the hints table"
  task :seed_hints => :environment do
    #newsitems
    Hint.create(:field => "newsitems-title", :text => "Use short titles if possible.")
    Hint.create(:field => "newsitems-lead", :text => "The lead will be displayed in the list of news and at the top of the news.")
    Hint.create(:field => "newsitems-content", :text => "Use textile syntax to format the text.")
    Hint.create(:field => "newsitems-published_on", :text => "This date is used to sort the news.")
    Hint.create(:field => "newsitems-category_id", :text => "Select the category that best fits or create a new one in the administration area.")
    
    #statics
    Hint.create(:field => "statics-content", :text => "Use textile syntax to format the text.")
    
    #partners
    Hint.create(:field => "partners-lead", :text => "The lead will appear in the list of partners. It is NOT included in the partner's description.")
    Hint.create(:field => "partners-url", :text => "Url must start with <em>http://</em>")
    
    #documents
    Hint.create(:field => "documents-title", :text => "Use the same title as in the document.")
    Hint.create(:field => "documents-subtitle", :text => "Use the same subtitle as in the document.")
    Hint.create(:field => "documents-document_published_on", :text => "The date when the document was published.")
    Hint.create(:field => "documents-abstract", :text => "Short description of the content of the document. Length should be between 75 and 100 words.")
    Hint.create(:field => "documents-category_id", :text => "Select the category that best fits or create a new one in the administration area.")
        
    #service providers
    Hint.create(:field => "service_providers-description", :text => "Specify all the details about the service provider including, but not only, the conditions to benefits from the services provided and the opening hours.")
    Hint.create(:field => "service_providers-address", :text => "Address must indicate precisely the location of the service provider.")
    Hint.create(:field => "service_providers-url", :text => "Url must start with <em>http://</em>")
    Hint.create(:field => "service_providers-category_id", :text => "Select the category that best fits or create a new one in the administration area.")
    Hint.create(:field => "service_providers-services", :text => "Select the services that are provided. Add missing ones in the administration area.")
    
    #links
    Hint.create(:field => "links-url", :text => "Url must start with <em>http://</em>")
    Hint.create(:field => "links-sequence", :text => "Order in which the links are displayed.")
  end
end