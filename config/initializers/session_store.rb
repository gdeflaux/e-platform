# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_e-platform_session',
  :secret      => 'ec5e639f0deaefdbebca0249e9c0459b88489fa5e1a2aa5b18d19d6f11f3a9a33c3c21006fc49aab439ebb76a1a1b06d62a74c564ce0246e992e4d99924e532a'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
