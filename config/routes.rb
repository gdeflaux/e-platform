ActionController::Routing::Routes.draw do |map|
  map.login "/login", :controller => "user_sessions", :action => "new"
  map.logout "/logout", :controller => "user_sessions", :action => "destroy"
  
  map.resources :user_sessions
  map.resources :users
  
  # Public routes to monthly_reports controller
  map.reports "/reports", :controller => "reporting/monthly_reports", :action => "reports_public_index"
  map.report "/reports/:year/:month", :controller => "reporting/monthly_reports", :action => "reports_public_show"
  
  map.service_providers_category "/service_providers/category/:category", :controller => "service_providers", :action => "index"
  map.resources :service_providers

  map.documents_category "/documents/category/:category", :controller => "documents", :action => "index"
  map.resources :documents

  map.resources :partners
  map.resources :links
  map.resources :statics

  map.newsitems_category "/newsitems/category/:category", :controller => "newsitems", :action => "index"
  map.resources :newsitems

  map.resources :assets

  map.namespace :admin do |admin|
    admin.new_admin "users/new_admin", :controller => "users", :action => "new_admin"
    admin.resources :users, :member => {:activate => :get, :deactivate => :get}
    admin.resources :services
    admin.resources :categories
    admin.resources :configurations
    admin.resources :languages, :collection => {:formated => :get}
    admin.root :controller => "users"
  end
  
  map.namespace :reporting do |reporting|
    reporting.resources :charts
    reporting.resources :monthly_reports
    reporting.resources :crc_members, :member => {:activate => :get, :deactivate => :get}
    reporting.resources :activities, :member => {:activate => :get, :deactivate => :get}
    reporting.resources :axes, :member => {:activate => :get, :deactivate => :get}    
    reporting.root :controller => "monthly_reports"
  end
  
  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing or commenting them out if you're using named routes and resources.
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
  map.root :controller => "newsitems"
end
